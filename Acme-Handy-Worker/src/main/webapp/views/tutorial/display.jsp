<%--
 * display.jsp
 *
 * Copyright (C) 2015 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>


<a title="Sponsorship" href="${sponsorship.targetPage}"><img src="${sponsorship.banner}" alt="Sponsorship" /></a>


<div style="position: relative; width: 500px; height: 250px;">



	<br> <spring:message code="tutorial.title" />:
	
	<jstl:out value="${tutorial.title}" />
	
	<br> <spring:message code="tutorial.moment" />:
	
	<jstl:out value="${tutorial.moment}" />
	
	<br> <spring:message code="tutorial.summary" />:
	
	<jstl:out value="${tutorial.summary}" />
	
	<br> <spring:message code="tutorial.pictures" />:
	
	<jstl:out value="${tutorial.picture}" />
	
	<br> <spring:message code="tutorial.sections" />:
	<a href="section/list.do?tutorialId=${tutorialId}">
			<spring:message code="tutorial.sections"/>
		</a>
	
	<br>
	<input type="button" name="cancel"
		value="<spring:message code="tutorial.back" />"
		onclick="javascript: relativeRedir('tutorial/list.do');" />	
	
</div>


</html>