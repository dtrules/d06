<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="customerEndorsement.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="customerEndorsement.display"/> </h2>

<security:authorize access="hasRole('CUSTOMER')">

	
	<!-- Attributes -->

	<b><spring:message code="customerEndorsement.score"/></b>: <jstl:out value="${customerEndorsement.score}"/>
	<br/>
	
	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('customerEndorsement/list.do')"
		value="<spring:message code="customerEndorsement.cancel" />" />

</security:authorize>

</html>
	
