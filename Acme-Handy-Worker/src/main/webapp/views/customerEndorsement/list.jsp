<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:customerEndorsement code="customerEndorsement.list" /></p>

<security:authorize access="hasRole('ADMIN')">

<!--Tabla-->

<display:table name="customerEndorsements" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->

	<security:authorize access="hasRole('CUSTOMER')">
	<display:column>
		<a href="customerEndorsement/edit.do?customerEndorsementId=${row.id}">
			<spring:customerEndorsement code="customerEndorsement.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="customerEndorsement/display.do?customerEndorsementId=${row.id}">
			<spring:customerEndorsement code="customerEndorsement.display"/>
		</a>
	</display:column>
	</security:authorize>
	<display:column property="score" titleKey="customerEndorsement.score" sortable="true"/>
</display:table>

</security:authorize>


<!--Bot�n de crear debajo de la lista-->

	<security:authorize access="isAuthenticated()">
	<div>
		<a href="customerEndorsement/create.do">
			<spring:customerEndorsement code="customerEndorsement.create"/>
		</a>
	</div>
	</security:authorize>