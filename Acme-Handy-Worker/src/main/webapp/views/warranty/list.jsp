<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="warranty.list" /></p>

<security:authorize access="hasRole('ADMIN')">

<!--Tabla-->

<display:table name="warranties" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de borrar y editar en cada fila-->

   
       <security:authorize access="hasRole('ADMIN')">
           <display:column titleKey="warranty.delete">
            <jstl:if test="${row.draftMode}">
                <a href="warranty/administrator/delete.do?warrantyId=${row.id}">
                    <spring:message code="warranty.delete"/>
                </a>
            </jstl:if>
           </display:column>
           <display:column titleKey="warranty.edit">
           	<jstl:if test="${row.draftMode}">
               <a href="warranty/administrator/edit.do?warrantyId=${row.id}">
                   <spring:message code="warranty.edit"/>
               </a>
             </jstl:if>
           </display:column>
       </security:authorize>
        
	<spring:message code="warranty.title" var="titleHeader"/>
	<display:column property="title" titleKey="warranty.title" sortable="true"/>

	<spring:message code="warranty.terms" var="termsHeader"/>
	<display:column property="terms" titleKey="warranty.terms" sortable="true"/>

	<spring:message code="warranty.laws" var="lawsHeader"/>
	<display:column property="laws" titleKey="warranty.laws" sortable="true"/>

	<spring:message code="warranty.draftMode" var="draftModeHeader"/>
	<display:column property="draftMode" titleKey="warranty.draftMode" sortable="true"/>
</display:table>

</security:authorize>



<!--Bot�n de crear debajo de la lista-->

<security:authorize access="hasRole('ADMIN')">
	<div>
		<a href="warranty/administrator/create.do">
			<spring:message code="warranty.create"/>
		</a>
	</div>
</security:authorize>
