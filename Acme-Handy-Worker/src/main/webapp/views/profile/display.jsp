<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="profile.display"/> </h2>

	<!-- Attributes -->

	<b><spring:message code="profile.nick"/></b>: <jstl:out value="${profile.nick}"/>
	<br/>
	<b><spring:message code="profile.socialNetwork"/></b>: <jstl:out value="${profile.socialNetwork}"/>
	<br/>
	<b><spring:message code="profile.link"/></b>: <jstl:out value="${profile.link}"/>
	<br/>

	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('profile/actor/list.do')"
		value="<spring:message code="profile.back" />" />

</html>
	
