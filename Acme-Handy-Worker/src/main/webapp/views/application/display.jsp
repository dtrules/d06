<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="application.display"/> </h2>

<spring:message code="application.date.pattern" var="datePattern"/>
	
	<!-- Attributes -->
	
	<div style="position: relative; width: 500px; height: 250px;">

	<b> <spring:message code="application.moment" />:
	</b>
	<jstl:out value="${application.moment}" />
	
	<b> <spring:message code="application.status" />:
	</b>
	<jstl:out value="${application.status}" />
	
	<b> <spring:message code="application.price" />:
	</b>
	<jstl:out value="${application.price}" />
	
	<b> <spring:message code="application.comments" />:
	</b>
	<jstl:forEach items="${application.comments}" var="comments" >
	<jstl:out value="${comments}" />
	</jstl:forEach>
	
	
	<input type="button" name="back"
		value="<spring:message code="application.back" />"
		onclick="javascript: relativeRedir('application/list.do');" />
	
</div>


</html>
	
