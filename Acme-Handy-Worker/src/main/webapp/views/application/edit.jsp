<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>



<form:form action="application/handyWorker/edit.do?applicationId=${applicationId }" modelAttribute="application" >

<!--Ocultos-->
<security:authorize access="hasRole('HANDYWORKER')">

    <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" />  
	<form:hidden path="status" />  
	<form:hidden path="comments" />




		  


	<!--Price-->
	<form:label path="price">
		<spring:message code="application.price"/>
    	</form:label>
	<form:input path="price"/>
    	<form:errors cssClass="error" path="price" />

	    <br/>
	    
	    
	        <!--Tasks-->
    <form:label path="task">
        <spring:message code="application.tasks"/>
        </form:label>
    <form:select id="tasks" path="task">
        <form:options items="${tasks}" itemLabel="ticker" itemValue="id"/>
        <form:option value="0" label="------"/>
    </form:select>
    <form:errors cssClass="error" path="task"/>
    <br/>	
	    </security:authorize>
	
	
	<!--  Botones de save y cancelar -->
	
	<input type="submit" name="save" value="<spring:message code="application.save"/>"/>
	

	
	<input type="button" name="cancel"
		value="<spring:message code="application.cancel" />"
		onclick="javascript: relativeRedir('application/list.do');" />
	
	

</form:form>
