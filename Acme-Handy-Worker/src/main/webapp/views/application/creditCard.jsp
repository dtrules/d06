<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('CUSTOMER')">

<form:form action="application/customer/creditCard.do" modelAttribute="application" >

 <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" />  
	<form:hidden path="price" />
	<form:hidden path="task" />
		
		
		

		
	        <!--Status-->
    <form:label path="status">
        <spring:message code="application.status"/>
        </form:label>
    <form:select id="status" path="status">
        <option value="${'ACCEPTED'}">
				<spring:message code="application.status.accepted"/>
			</option>
			<option value="${'REJECTED'}">
				<spring:message code="application.status.rejected"/>
			</option>
    </form:select>
    <form:errors cssClass="error" path="status"/>
    <br/>	
	

	<!-- HolderName-->
    	<form:label path="creditCard.holderName">
		<spring:message code="creditCard.holderName"/>
    	</form:label>
    	<form:input path="creditCard.holderName" />
    	<form:errors cssClass="error" path="creditCard.holderName" />
	<br/>
	
		<!-- BrandName-->
    	<form:label path="creditCard.brandName">
		<spring:message code="creditCard.brandName"/>
    	</form:label>
    	<form:input path="creditCard.brandName" />
    	<form:errors cssClass="error" path="creditCard.brandName" />
	<br/>
	
		<!-- Number-->
    	<form:label path="creditCard.number">
		<spring:message code="creditCard.number"/>
    	</form:label>
    	<form:input path="creditCard.number" />
    	<form:errors cssClass="error" path="creditCard.number" />
	<br/>
	
		<!-- Expiration Month-->
    	<form:label path="creditCard.expirationMonth">
		<spring:message code="creditCard.expirationMonth"/>
    	</form:label>
    	<form:input path="creditCard.expirationMonth" />
    	<form:errors cssClass="error" path="creditCard.expirationMonth" />
	<br/>
	
		<!-- Expiration Year-->
    	<form:label path="creditCard.expirationYear">
		<spring:message code="creditCard.expirationYear"/>
    	</form:label>
    	<form:input path="creditCard.expirationYear" />
    	<form:errors cssClass="error" path="creditCard.expirationYear" />
	<br/>
	
			<!-- CVVCode-->
    	<form:label path="creditCard.cvvCode">
		<spring:message code="creditCard.cvvCode"/>
    	</form:label>
    	<form:input path="creditCard.cvvCode" />
    	<form:errors cssClass="error" path="creditCard.cvvCode" />
	<br/>


			<!-- Comments-->
    	<form:label path="comments">
		<spring:message code="application.comments"/>
    	</form:label>
    	<form:input path="comments" />
    	<form:errors cssClass="error" path="comments" />
	<br/>
	

	<!--  Botones de save y cancelar -->
	
	<input type="submit" name="save" value="<spring:message code="application.save"/>"/>
	
	<input type="button" name="cancel"
		value="<spring:message code="application.cancel" />"
		onclick="javascript: relativeRedir('application/customer/list.do');" />
	
	

</form:form>
</security:authorize>
