<%--
 * display.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="sponsor.display"/> </h2>
	
	<!-- Attributes -->

	<b><spring:message code="sponsor.name"/>:</b><jstl:out value="${sponsor.name}"/>
	<br/>
    <b><spring:message code="sponsor.middleName"/>:</b><jstl:out value="${sponsor.middleName}"/>
    <br/>
  	<b><spring:message code="sponsor.surname"/>:</b><jstl:out value="${sponsor.surname}"/>
  	<br/>
	<b><spring:message code="sponsor.photo"/>:</b><jstl:out value="${sponsor.photo}"/>
	<br/>
	<b><spring:message code="sponsor.email"/>:</b><jstl:out value="${sponsor.email}"/>
	<br/>
	<b><spring:message code="sponsor.phoneNumber"/>:</b><jstl:out value="${sponsor.phoneNumber}"/>
	<br/>
	<b><spring:message code="sponsor.address"/>:</b><jstl:out value="${sponsor.address}"/>
	<br/>
	<b><spring:message code="sponsor.username"/>:</b><jstl:out value="${sponsor.username}"/>
	<br/>
	<b><spring:message code="sponsor.password"/>:</b><jstl:out value="${sponsor.password}"/>
	<br/>
	
	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('sponsor/list.do')"
		value="<spring:message code="sponsor.cancel" />" />

</html>
	