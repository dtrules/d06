<%--
 * list.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="sponsor.list" /></p>

<!--Tabla-->

<display:table name="sponsors" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->

	<security:authorize access="hasRole('ADMIN')">
	
	<display:column>
		<a href="sponsor/edit.do?sponsorId=${row.id}">
			<spring:message code="sponsor.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="sponsor/display.do?sponsorId=${row.id}">
			<spring:message code="sponsor.display"/>
		</a>
	</display:column>
	</security:authorize>
	
	<display:column property="name" titleKey="sponsor.name" sortable="true"/>
	<display:column property="middleName" titleKey="sponsor.middleName" sortable="true"/>
	<display:column property="surname" titleKey="sponsor.surname" sortable="true"/>
	<display:column property="photo" titleKey="sponsor.photo" sortable="true"/>
	<display:column property="email" titleKey="sponsor.email" sortable="true"/>
	<display:column property="phoneNumber" titleKey="sponsor.phoneNumber" sortable="true"/>
	<display:column property="address" titleKey="sponsor.address" sortable="true"/>
	<display:column property="username" titleKey="sponsor.username" sortable="true"/>
	<display:column property="password" titleKey="sponsor.password" sortable="true"/>
</display:table>