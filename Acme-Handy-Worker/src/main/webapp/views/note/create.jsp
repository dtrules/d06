<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="note.create" /></p>

<security:authorize access="isAuthenticated()">

<form:form action="note/create.do?=reportId" modelAttribute="note">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" /> 
	
    	
    	
    <!--customerComments-->
	<form:label path="customerComments">
		<spring:message code="note.customerComments"/>
    	</form:label>
	<form:textarea path="customerComments"/>
    	<form:errors cssClass="error" path="customerComments" />
    	
    	<!--refereeComments-->
	<form:label path="refereeComments">
		<spring:message code="note.refereeComments"/>
    	</form:label>
	<form:textarea path="refereeComments"/>
    	<form:errors cssClass="error" path="refereeComments" />
    	
    	<!--handyWorkerComments-->
	<form:label path="handyWorkerComments">
		<spring:message code="note.handyWorkerComments"/>
    	</form:label>
	<form:textarea path="handyWorkerComments"/>
    	<form:errors cssClass="error" path="handyWorkerComments" />
    	
	
	<input type="submit" name="save"
		value="<spring:message code="note.save" />" 
		onclick="return confirm('<spring:message code="note.confirm.save" />')" />&nbsp;
		
		
	
	<input type="button" name="cancel"
		value="<spring:message code="note.cancel" />"
		onclick="javascript: relativeRedir('task/list.do');" />
	<br />

</form:form>
</security:authorize>
