<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('SPONSOR')">

	<form:form action="sponsorship/sponsor/create.do"
		modelAttribute="sponsorship">

		<!--Ocultos-->

		<form:hidden path="id" />
		<form:hidden path="version" />



		<!--Banner-->
		<form:label path="banner">
			<spring:message code="sponsorship.banner" />
		</form:label>
		<form:input path="banner" />
		<form:errors cssClass="error" path="banner" />

		<br />

		<!--TargetPage-->
		<form:label path="targetPage">
			<spring:message code="sponsorship.targetPage" />
		</form:label>
		<form:input path="targetPage" />
		<form:errors cssClass="error" path="targetPage" />

		<br />
		
		<!--Tutorial-->
    <form:label path="tutorial">
        <spring:message code="sponsorship.tutorial"/>
        </form:label>
    <form:select id="tutorials" path="tutorial">
        <form:options items="${tutorials}" itemLabel="title" itemValue="id"/>
        <form:option value="0" label="------"/>
    </form:select>
    <form:errors cssClass="error" path="tutorial"/>
    <br/>	
    
    <!--CreditCard-->
<spring:message code="sponsorship.creditCard"/>:
		<fieldset>	
		
		
		<!-- HolderName-->
    	<form:label path="creditCard.holderName">
		<spring:message code="creditCard.holderName"/>
    	</form:label>
    	<form:input path="creditCard.holderName" />
    	<form:errors cssClass="error" path="creditCard.holderName" />
	<br/>
	
		<!-- BrandName-->
    	<form:label path="creditCard.brandName">
		<spring:message code="creditCard.brandName"/>
    	</form:label>
    	<form:input path="creditCard.brandName" />
    	<form:errors cssClass="error" path="creditCard.brandName" />
	<br/>
	
		<!-- Number-->
    	<form:label path="creditCard.number">
		<spring:message code="creditCard.number"/>
    	</form:label>
    	<form:input path="creditCard.number" />
    	<form:errors cssClass="error" path="creditCard.number" />
	<br/>
	
		<!-- Expiration Month-->
    	<form:label path="creditCard.expirationMonth">
		<spring:message code="creditCard.expirationMonth"/>
    	</form:label>
    	<form:input path="creditCard.expirationMonth" />
    	<form:errors cssClass="error" path="creditCard.expirationMonth" />
	<br/>
	
		<!-- Expiration Year-->
    	<form:label path="creditCard.expirationYear">
		<spring:message code="creditCard.expirationYear"/>
    	</form:label>
    	<form:input path="creditCard.expirationYear" />
    	<form:errors cssClass="error" path="creditCard.expirationYear" />
	<br/>
	
			<!-- CVVCode-->
    	<form:label path="creditCard.cvvCode">
		<spring:message code="creditCard.cvvCode"/>
    	</form:label>
    	<form:input path="creditCard.cvvCode" />
    	<form:errors cssClass="error" path="creditCard.cvvCode" />
	<br/>

	

		</fieldset>




		<input type="submit" name="save"
			value="<spring:message code="sponsorship.save"/>" />

		<input type="button" name="cancel"
			value="<spring:message code="sponsorship.cancel" />"
			onclick="javascript: relativeRedir('tutorial/list.do');" />

	</form:form>
</security:authorize>
