<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:handyWorker code="handyWorker.list" /></p>

<!--Tabla-->

<display:table name="handyWorkers" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->

	<security:authorize access="hasRole('ADMIN')">
	<display:column>
		<a href="handyWorker/edit.do?handyWorkerId=${row.id}">
			<spring:handyWorker code="handyWorker.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="handyWorker/display.do?handyWorkerId=${row.id}">
			<spring:handyWorker code="handyWorker.display"/>
		</a>
	</display:column>
	</security:authorize>
	<display:column property="name" titleKey="handyWorker.name" sortable="true"/>
	<display:column property="middleName" titleKey="handyWorker.middleName" sortable="true"/>
	<display:column property="surname" titleKey="handyWorker.surname" sortable="true"/>
	<display:column property="photo" titleKey="handyWorker.photo" sortable="true"/>
	<display:column property="email" titleKey="handyWorker.email" sortable="true"/>
	<display:column property="phoneNumber" titleKey="handyWorker.phoneNumber" sortable="true"/>
	<display:column property="address" titleKey="handyWorker.address" sortable="true"/>
	<display:column property="username" titleKey="handyWorker.username" sortable="true"/>
	<display:column property="password" titleKey="handyWorker.password" sortable="true"/>
</display:table>

<!--Bot�n de crear debajo de la lista-->

	<security:authorize access="isAuthenticated()">
	<div>
		<a href="handyWorker/create.do">
			<spring:handyWorker code="handyWorker.create"/>
		</a>
	</div>
	</security:authorize>