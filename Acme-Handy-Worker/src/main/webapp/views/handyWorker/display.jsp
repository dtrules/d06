<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="handyWorker.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="handyWorker.display"/> </h2>
	
	<!-- Attributes -->

	<b><spring:message code="handyWorker.name"/></b>: <jstl:out value="${handyWorker.name}"/>
	<br/>
    <b><spring:message code="handyWorker.middleName"/></b>: <jstl:out value="${handyWorker.middleName}"/>
    <br/>
  	<b><spring:message code="handyWorker.surname"/></b>: <jstl:out value="${handyWorker.surname}"/>
  	<br/>
	<b><spring:message code="handyWorker.photo"/></b>: <jstl:out value="${handyWorker.photo}"/>
	<br/>
	<b><spring:message code="handyWorker.email"/></b>: <jstl:out value="${handyWorker.email}"/>
	<br/>
	<b><spring:message code="handyWorker.phoneNumber"/></b>: <jstl:out value="${handyWorker.phoneNumber}"/>
	<br/>
	<b><spring:message code="handyWorker.address"/></b>: <jstl:out value="${handyWorker.address}"/>
	<br/>
	<b><spring:message code="handyWorker.username"/></b>: <jstl:out value="${handyWorker.username}"/>
	<br/>
	<b><spring:message code="handyWorker.password"/></b>: <jstl:out value="${handyWorker.password}"/>
	<br/>
	
	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('handyWorker/list.do')"
		value="<spring:message code="handyWorker.cancel" />" />

</html>
	
