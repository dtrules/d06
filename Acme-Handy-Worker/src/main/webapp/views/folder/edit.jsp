<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="isAuthenticated()">

<form:form action="folder/actor/edit.do" modelAttribute="folder">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
    <form:hidden path="messages" />  

	<!--Name-->
	<form:label path="name">
		<spring:message code="folder.name"/>
    	</form:label>
	<form:input path="name"/>
    	<form:errors cssClass="error" path="name" />

	
	<input type="submit" name="save" value="<spring:message code="folder.save"/>"/>
	
	<jstl:if test = "${folder.id!=0}">
			<input type="submit" name="delete" onclick="return confirm('<spring:message code="folder.edit.confirm.delete"/>')"
				value="<spring:message code="folder.delete"/>" />
			&nbsp;
		</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="folder.cancel" />"
		onclick="javascript: relativeRedir('folder/actor/list.do');" />	

</form:form>
</security:authorize>
