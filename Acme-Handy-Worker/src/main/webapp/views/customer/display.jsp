<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="customer.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="customer.display"/> </h2>
	
	<!-- Attributes -->

	<b><spring:message code="customer.name"/></b>: <jstl:out value="${customer.name}"/>
	<br/>
    <b><spring:message code="customer.middleName"/></b>: <jstl:out value="${customer.middleName}"/>
    <br/>
  	<b><spring:message code="customer.surname"/></b>: <jstl:out value="${customer.surname}"/>
  	<br/>
	<b><spring:message code="customer.photo"/></b>: <jstl:out value="${customer.photo}"/>
	<br/>
	<b><spring:message code="customer.email"/></b>: <jstl:out value="${customer.email}"/>
	<br/>
	<b><spring:message code="customer.phoneNumber"/></b>: <jstl:out value="${customer.phoneNumber}"/>
	<br/>
	<b><spring:message code="customer.address"/></b>: <jstl:out value="${customer.address}"/>
	<br/>
	<b><spring:message code="customer.username"/></b>: <jstl:out value="${customer.username}"/>
	<br/>
	<b><spring:message code="customer.password"/></b>: <jstl:out value="${customer.password}"/>
	<br/>
	
	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('customer/list.do')"
		value="<spring:message code="customer.cancel" />" />

</html>
	
