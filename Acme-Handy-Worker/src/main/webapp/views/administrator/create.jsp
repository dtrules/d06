<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 
<security:authorize access="hasRole('ADMIN')">

<form:form action="administrator/create.do" modelAttribute="administrator">

	<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />
    <form:hidden path="userAccount" />
    <form:hidden path="userAccount.authorities" />
    
    <!--Name-->
	<form:label path="name">
		<spring:message code="administrator.name"/>
    	</form:label>
	<form:input path="name"/>
    	<form:errors cssClass="error" path="name" />
    <br>

    <!--MiddleName-->
	<form:label path="middleName">
		<spring:message code="administrator.middleName"/>
    	</form:label>
	<form:input path="middleName"/>
    	<form:errors cssClass="error" path="middleName" />
    <br>
    	
    <!--Surname-->
	<form:label path="surname">
		<spring:message code="administrator.surname"/>
    	</form:label>
	<form:input path="surname"/>
    	<form:errors cssClass="error" path="surname" />
    <br>
    	
    <!--Photo-->
	<form:label path="photo">
		<spring:message code="administrator.photo"/>
    	</form:label>
	<form:input path="photo"/>
    	<form:errors cssClass="error" path="photo" />
    <br>
    	
    <!--Email-->
	<form:label path="email">
		<spring:message code="administrator.email"/>
    	</form:label>
	<form:input path="email"/>
    	<form:errors cssClass="error" path="email" />
    <br>
    	
    <!--PhoneNumber-->
	<form:label path="phoneNumber">
		<spring:message code="administrator.phoneNumber"/>
    	</form:label>
	<form:input path="phoneNumber"/>
    	<form:errors cssClass="error" path="phoneNumber" />
    <br>
    	
    <!--Address-->
	<form:label path="address">
		<spring:message code="administrator.address"/>
    	</form:label>
	<form:input path="address"/>
    	<form:errors cssClass="error" path="address" />
    <br>
    	
    <!--Username-->
	<form:label path="userAccount.username">
		<spring:message code="administrator.username"/>
    	</form:label>
	<form:input path="userAccount.username"/>
    	<form:errors cssClass="error" path="userAccount.username" />
    <br>
    	
    <!--Password-->
	<form:label path="userAccount.password">
		<spring:message code="administrator.password"/>
    	</form:label>
	<form:input path="userAccount.password"/>
    	<form:errors cssClass="error" path="userAccount.password" />
    <br>
    <br>
    	
    <input type="submit" name="save" value="<spring:message code="administrator.save"/>"/>
	<input type="submit" name="cancel" value="<spring:message code="administrator.cancel"/>"/>
	<br/>

</form:form>
</security:authorize>