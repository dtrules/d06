<%--
 * list.jsp
 *
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- The minimum, the maximum, the average, and the standard deviation of the number of complaints per fix-up task. -->
<p>	<span><spring:message code="dashboard.1" />:</span> <br/>
<spring:message code="dashboard.1.min" />:&nbsp; <jstl:out value="${minNumberOfComplaints}"></jstl:out><br/>
<spring:message code="dashboard.1.avg" />:&nbsp; <jstl:out value="${avgNumberOfComplaints}"></jstl:out><br/>
<spring:message code="dashboard.1.max" />:&nbsp; <jstl:out value="${maxNumberOfComplaints}"></jstl:out><br/>
<spring:message code="dashboard.1.deviation" />:&nbsp; <jstl:out value="${deviationNumberOfComplaints}"></jstl:out><br/>

<!-- The minimum, the maximum, the average, and the standard deviation of the number of notes per referee report. -->
<p>	<span><spring:message code="dashboard.2" />:</span> <br/>
<spring:message code="dashboard.2.min" />:&nbsp; <jstl:out value="${minNotesPerReport}"></jstl:out><br/>
<spring:message code="dashboard.2.avg" />:&nbsp; <jstl:out value="${avgNotesPerReport}"></jstl:out><br/>
<spring:message code="dashboard.2.max" />:&nbsp; <jstl:out value="${maxNotesPerReport}"></jstl:out><br/>
<spring:message code="dashboard.2.deviation" />:&nbsp; <jstl:out value="${deviationNotesPerReport}"></jstl:out><br/>

<!-- The average, the minimum, the maximum, and the standard deviation of the number of fix-up tasks per user. -->
<p>	<span><spring:message code="dashboard.3" />:</span> <br/>
<spring:message code="dashboard.3.min" />:&nbsp; <jstl:out value="${minTasksPerUser}"></jstl:out><br/>
<spring:message code="dashboard.3.avg" />:&nbsp; <jstl:out value="${avgTasksPerUser}"></jstl:out><br/>
<spring:message code="dashboard.3.max" />:&nbsp; <jstl:out value="${maxTasksPerUser}"></jstl:out><br/>
<spring:message code="dashboard.3.deviation" />:&nbsp; <jstl:out value="${deviationTasksPerUser}"></jstl:out><br/>

<!-- The average, the minimum, the maximum, and the standard deviation of the number of applications per fix-up task. -->
<p>	<span><spring:message code="dashboard.4" />:</span> <br/>
<spring:message code="dashboard.4.min" />:&nbsp; <jstl:out value="${minApplicationsPerTask}"></jstl:out><br/>
<spring:message code="dashboard.4.avg" />:&nbsp; <jstl:out value="${avgApplicationsPerTask}"></jstl:out><br/>
<spring:message code="dashboard.4.max" />:&nbsp; <jstl:out value="${maxApplicationsPerTask}"></jstl:out><br/>
<spring:message code="dashboard.4.deviation" />:&nbsp; <jstl:out value="${deviationApplicationsPerTask}"></jstl:out><br/>

<!-- The average, the minimum, the maximum, and the standard deviation of the maximum price of the fix-up tasks. -->
<p>	<span><spring:message code="dashboard.5" />:</span> <br/>
<spring:message code="dashboard.5.min" />:&nbsp; <jstl:out value="${minMaxPricePerTask}"></jstl:out><br/>
<spring:message code="dashboard.5.avg" />:&nbsp; <jstl:out value="${avgMaxPricePerTask}"></jstl:out><br/>
<spring:message code="dashboard.5.max" />:&nbsp; <jstl:out value="${maxMaxPricePerTask}"></jstl:out><br/>
<spring:message code="dashboard.5.deviation" />:&nbsp; <jstl:out value="${deviationMaxPricePerTask}"></jstl:out><br/>

<!-- The average, the minimum, the maximum, and the standard deviation of the price offered in the applications. -->
<p>	<span><spring:message code="dashboard.6" />:</span> <br/>
<spring:message code="dashboard.6.min" />:&nbsp; <jstl:out value="${minPriceInApplications}"></jstl:out><br/>
<spring:message code="dashboard.6.avg" />:&nbsp; <jstl:out value="${avgPriceInApplications}"></jstl:out><br/>
<spring:message code="dashboard.6.max" />:&nbsp; <jstl:out value="${maxPriceInApplications}"></jstl:out><br/>
<spring:message code="dashboard.6.deviation" />:&nbsp; <jstl:out value="${deviationPriceInApplications}"></jstl:out><br/>

<!-- The ratio of fix-up tasks with a complaint. -->
<spring:message code="dashboard.7"/>:&nbsp; <jstl:out value="${ratioOfTasksWithAComplaint}"></jstl:out><br/>

<!-- The ratio of pending applications. -->
<spring:message code="dashboard.8"/>:&nbsp; <jstl:out value="${ratioOfPendingApplications}"></jstl:out><br/>

<!-- The ratio of accepted applications. -->
<spring:message code="dashboard.9"/>:&nbsp; <jstl:out value="${ratioOfAcceptedApplications}"></jstl:out><br/>

<!-- The ratio of rejected applications. -->
<spring:message code="dashboard.10"/>:&nbsp; <jstl:out value="${ratioOfRejectedApplications}"></jstl:out><br/>

<!-- The ratio of pending applications that cannot change its status because their time periods elapsed. -->
<spring:message code="dashboard.11"/>:&nbsp; <jstl:out value="${ratioOfElapsedPendingApplications}"></jstl:out><br/>

<!-- The listing of customers who have published at least 10% more fix-up tasks than the average, ordered by number of applications. -->
<p>	<span><spring:message code="dashboard.12" />:</span><br/>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="listCustomersMoreTasks" requestURI="dashboard/administrator/list.do" id="row">
  	<spring:message code="dashboard.customer.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
	<a href="customer/list.do?customerId=<jstl:out value="${row.id}"/>"><jstl:out value="${row.title}"/></a>
	</display:column>	 	
</display:table> 
</p>

<!-- The listing of handy workers who have got accepted at least 10% more applications than the average, ordered by number of applications. -->
<p>	<span><spring:message code="dashboard.13" />:</span><br/>
<display:table pagesize="5" class="displaytag" keepStatus="true" name="listHandyWorkersMoreApplications" requestURI="dashboard/administrator/list.do" id="row">
  	<spring:message code="dashboard.handyworker.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
	<a href="handyWorker/list.do?handyWorkerId=<jstl:out value="${row.id}"/>"><jstl:out value="${row.title}"/></a>
	</display:column>	 	
</display:table> 
</p>

<!-- The top-three customers in terms of complaints. -->
<p>	<span><spring:message code="dashboard.14" />:</span><br/>
<display:table pagesize="3" class="displaytag" keepStatus="true" name="listTopThreeCustomers" requestURI="dashboard/administrator/list.do" id="row">
  	<spring:message code="dashboard.customer.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
	<a href="customer/list.do?customerId=<jstl:out value="${row.id}"/>"><jstl:out value="${row.title}"/></a>
	</display:column>	 	
</display:table> 
</p>

<!-- The top-three handy workers in terms of complaints. -->
<p>	<span><spring:message code="dashboard.15" />:</span><br/>
<display:table pagesize="3" class="displaytag" keepStatus="true" name="listTopThreeHandyWorkers" requestURI="dashboard/administrator/list.do" id="row">
  	<spring:message code="dashboard.handyworker.name" var="nameHeader" />
	<display:column title="${nameHeader}" sortable="true">
	<a href="handyWorker/list.do?handyWorkerId=<jstl:out value="${row.id}"/>"><jstl:out value="${row.title}"/></a>
	</display:column>	 	
</display:table> 
</p>

<input type="button" name="back"
		value="<spring:message code="dashboard.back" />"
		onclick="javascript: relativeRedir('/');" />