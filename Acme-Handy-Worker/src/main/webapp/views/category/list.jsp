<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<p><spring:message code="category.list" /></p>

<security:authorize access="hasRole('ADMIN')">

<!--Tabla-->

<display:table name="categories" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de borrar y editar en cada fila-->

	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="category/administrator/delete.do?categoryId=${row.id}">
				<spring:message code="category.delete"/>
			</a>
		</display:column>
		<display:column>
			<a href="category/administrator/edit.do?categoryId=${row.id}">
				<spring:message code="category.edit"/>
			</a>
		</display:column>
	</security:authorize>
	<display:column>
		<a href="category/administrator/display.do?categoryId=${row.id}">
			<spring:message code="category.display"/>
		</a>
	</display:column>
	
	<jstl:if test="${pageContext.response.locale == 'en'}">
	<display:column property="name" titleKey="category.name" sortable="true"/>
	</jstl:if>
	
	<jstl:if test="${pageContext.response.locale == 'es'}">
	<display:column property="nombre" titleKey="category.nombre" sortable="true"/>
	</jstl:if>
	
</display:table>

</security:authorize>

<!--Bot�n de crear debajo de la lista-->

<security:authorize access="hasRole('ADMIN')">
	<div>
		<a href="category/administrator/create.do">
			<spring:message code="category.create"/>
		</a>
	</div>
</security:authorize>
