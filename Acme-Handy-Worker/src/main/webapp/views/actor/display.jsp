<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<div style="position: relative; width: 500px; height: 250px;">

	<b> <spring:message code="actor.name"/>:
	</b>
	<jstl:out value="${actor.name}"/>
	
	<b> <spring:message code="actor.middleName"/>:
	</b>
	<jstl:out value="${actor.middleName}"/>
	
	<b> <spring:message code="actor.surname"/>:
	</b>
	<jstl:out value="${actor.surname}"/>
	
	<b> <spring:message code="actor.photo"/>:
	</b>
	<jstl:out value="${actor.photo}"/>
	
	<b> <spring:message code="actor.email"/>:
	</b>
	<jstl:out value="${actor.email}"/>
	
	<b> <spring:message code="actor.phoneNumber"/>:
	</b>
	<jstl:out value="${actor.phoneNumber}"/>
	
	<b> <spring:message code="actor.address"/>:
	</b>
	<jstl:out value="${actor.address}"/>
	
	<b> <spring:message code="actor.suspicious"/>:
	</b>
	<jstl:out value="${actor.suspicious}"/>
	
	<b> <spring:message code="actor.banned"/>:
	</b>
	<jstl:out value="${actor.banned}"/>
	
	<b> <spring:message code="actor.profiles"/>:
	</b>
	<jstl:out value="${actor.profiles}"/>
	
	<b> <spring:message code="actor.folders"/>:
	</b>
	<jstl:out value="${actor.folders}"/>
	
	<b> <spring:message code="actor.username"/>:
	</b>
	<jstl:out value="${actor.username}"/>
	
	<b> <spring:message code="actor.password"/>:
	</b>
	<jstl:out value="${actor.password}"/>
	
	<input type="submit" name="back" value="<spring:message code="actor.back"/>"/>
	
</div>
