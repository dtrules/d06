<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="customization/administrator/edit.do" modelAttribute="customization">

	<form:hidden path="id" />
	<form:hidden path="version" />

	<form:label path="header">
		<spring:message code="customization.header" />:
	</form:label>
	<form:input path="header" />
	<form:errors cssClass="error" path="header" />
	<br />

	<form:label path="welcomeMessage">
		<spring:message code="customization.welcomeMessage" />:
	</form:label>
	<form:input path="welcomeMessage" />
	<form:errors cssClass="error" path="welcomeMessage" />
	<br />
	
		<form:label path="spamWords">
		<spring:message code="customization.spamWords" />:
	</form:label>
	<form:input path="spamWords" />
	<form:errors cssClass="error" path="spamWords" />
	<br />
	
		<form:label path="vat">
		<spring:message code="customization.vat" />:
	</form:label>
	<form:input path="vat" />
	<form:errors cssClass="error" path="vat" />
	<br />
	
	<form:label path="countryCode">
		<spring:message code="customization.countryCode" />:
	</form:label>
	<form:input path="countryCode" />
	<form:errors cssClass="error" path="countryCode" />
	<br />
	
	<form:label path="brands">
		<spring:message code="customization.brands" />:
	</form:label>
	<form:input path="brands" />
	<form:errors cssClass="error" path="brands" />
	<br />
	
	<form:label path="finderCache">
		<spring:message code="customization.finderCache" />:
	</form:label>
	<form:input path="finderCache" />
	<form:errors cssClass="error" path="finderCache" />
	<br />
	
	<form:label path="maxFinderResults">
		<spring:message code="customization.maxFinderResults" />:
	</form:label>
	<form:input path="maxFinderResults" />
	<form:errors cssClass="error" path="maxFinderResults" />
	<br />
	
	<form:label path="positiveWords">
		<spring:message code="customization.positiveWords" />:
	</form:label>
	<form:input path="positiveWords" />
	<form:errors cssClass="error" path="positiveWords" />
	<br />
	
	<form:label path="negativeWords">
		<spring:message code="customization.negativeWords" />:
	</form:label>
	<form:input path="negativeWords" />
	<form:errors cssClass="error" path="negativeWords" />
	<br />


	<input type="submit" name="save"
		value="<spring:message code="customization.save" />" />&nbsp; 
	<input type="button" name="cancel"
		value="<spring:message code="customization.cancel" />"
		onclick="javascript: relativeRedir('');" />
	<br />


</form:form>