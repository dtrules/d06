<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="handyWorkerEndorsement.create" /></p>

<security:authorize access="hasRole('HANDYWORKER')">

<form:form action="handyWorkerEndorsement/handyWorker/create.do" modelAttribute="handyWorkerEndorsement">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  

	<!--Score-->
	<form:label path="score">
		<spring:message code="handyWorkerEndorsement.score"/>
    	</form:label>
	<form:input path="score"/>
    	<form:errors cssClass="error" path="score" />
    	

	<input type="submit" name="save" value="<spring:message code="handyWorkerEndorsement.save"/>"/>

	<input type="submit" name="cancel" value="<spring:message code="handyWorkerEndorsement.cancel"/>"/>
	<br/>

</form:form>
</security:authorize>
