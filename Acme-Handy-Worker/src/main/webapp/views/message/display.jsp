<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>


	
	<!-- Attributes -->
	
	<br> <spring:message code="message.moment" />:
	
	<jstl:out value="${newMessage.moment}" />
	<br/>
	
	<spring:message code="message.sender"/>:
	 <jstl:out value="${newMessage.sender.name}"/>
    <br/>
    
    <spring:message code="message.recipient"/></b>:
     <jstl:out value="${newMessage.recipient.name}"/>
    <br/>
    
	<spring:message code="message.subject"/></b>:
	 <jstl:out value="${newMessage.subject}"/>
    <br/>
    
    <spring:message code="message.body"/></b>: 
    <jstl:out value="${newMessage.body}"/>
    <br/>
    
	<spring:message code="message.priority"/></b>: 
	<jstl:out value="${newMessage.priority.value}"/>
	<br/>
	
	<spring:message code="message.tags"/></b>: 
	<jstl:out value="${newMessage.tags}"/>
	<br/>

	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('folder/actor/listSystem.do')"
		value="<spring:message code="message.cancel" />" />	

</html>
	
