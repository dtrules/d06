<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- Listing grid -->

	<security:authorize access="hasRole('HANDYWORKER')">
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="professionalRecords" requestURI="${requestURI}" id="row">
	
	<!-- Action links -->

	<%-- <security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="announcement/administrator/edit.do?announcementId=${row.id}">
				<spring:message	code="announcement.edit" />
			</a>
		</display:column>		
	</security:authorize> --%>

	
	<!-- Attributes -->
	
	<spring:message code="professionalRecord.company" var="companyHeader" />
	<display:column property="company" title="${companyHeader}" sortable="true" />
	
	<spring:message code="professionalRecord.startMoment" var="startMomentHeader" />
	<display:column property="startMoment" title="${startMomentHeader}" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>
	
	<spring:message code="professionalRecord.endMoment" var="endMomentHeader" />
	<display:column property="endMoment" title="${endMomentHeader}" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>

	<spring:message code="professionalRecord.role" var="roleHeader" />
	<display:column property="role" title="${roleHeader}" sortable="true" />

	<spring:message code="professionalRecord.comments" var="commentsHeader" />
	<display:column property="comments" title="${commentsHeader}" sortable="true" />
	
	<spring:message code="professionalRecord.attachment" var="attachmentHeader" />
	<display:column property="attachment" title="${attachmentHeader}" sortable="true" />

</display:table>
</security:authorize>

<!-- Action links -->

<security:authorize access="hasRole('HANDYWORKER')">
	<div>
		<a href="professionalRecord/handyWorker/create.do"> <spring:message
				code="professionalRecord.create" />
		</a>
	</div>
</security:authorize>