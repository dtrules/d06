<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<form:form action="professionalRecord/handyWorker/create.do" modelAttribute="professionalRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />

	<form:label path="company">
		<spring:message code="professionalRecord.company" />:
	</form:label>
	<form:input path="company" />
	<form:errors cssClass="error" path="company" />
	<br />

	<form:label path="startMoment">
		<spring:message code="professionalRecord.startMoment" />:
	</form:label>
	<form:input type="datetime" path="startMoment" />
	<form:errors cssClass="error" path="startMoment" />
	<br />
	
	<form:label path="endMoment">
		<spring:message code="professionalRecord.endMoment" />:
	</form:label>
	<form:input type="datetime" path="endMoment" />
	<form:errors cssClass="error" path="endMoment" />
	<br />

	<form:label path="role">
		<spring:message code="professionalRecord.role" />:
	</form:label>
	<form:input path="role" />
	<form:errors cssClass="error" path="role" />
	<br />
	
	<form:label path="comments">
		<spring:message code="professionalRecord.comments" />:
	</form:label>
	<form:textarea path="comments" />
	<form:errors cssClass="error" path="comments" />
	<br />
	
		<form:label path="attachment">
		<spring:message code="professionalRecord.attachment" />:
	</form:label>
	<form:input path="attachment" />
	<form:errors cssClass="error" path="attachment" />
	<br />
	


	<input type="submit" name="save"
		value="<spring:message code="professionalRecord.save" />" />&nbsp; 
	<jstl:if test="${educationRecord.id != 0}">
		<input type="submit" name="delete"
			value="<spring:message code="professionalRecord.delete" />"
			onclick="return confirm('<spring:message code="professionalRecord.confirm.delete" />')" />&nbsp;
	</jstl:if>
	<input type="button" name="cancel"
		value="<spring:message code="professionalRecord.cancel" />"
		onclick="javascript: relativeRedir('professionalRecord/handyWorker/list.do');" />
	<br />


</form:form>