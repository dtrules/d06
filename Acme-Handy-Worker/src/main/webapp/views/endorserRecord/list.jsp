<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- Listing grid -->

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="miscellaneousRecords" requestURI="${requestURI}" id="row">
	
	<!-- Action links -->

	
	<security:authorize access="hasRole('HANDYWORKER')">
		<display:column>
			<a href="miscellaneousRecord/handyWorker/edit.do?miscellaneousRecordId=${row.id}">
				<spring:message	code="miscellaneousRecord.edit" />
			</a>
		</display:column>		
	</security:authorize>

	
	<!-- Attributes -->
	
	<spring:message code="miscellaneousRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="miscellaneousRecord.comments" var="commentsHeader" />
	<display:column property="comments" title="${commentsHeader}" sortable="true" />
	
	<spring:message code="miscellaneousRecord.attachment" var="attachmentHeader" />
	<display:column property="attachment" title="${attachmentHeader}" sortable="true" />

</display:table>


<!-- Action links -->

<security:authorize access="hasRole('HANDYWORKER')">
	<div>
		<a href="miscellaneousRecord/handyWorker/create.do"> <spring:message
				code="miscellaneousRecord.create" />
		</a>
	</div>
</security:authorize>