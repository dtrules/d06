<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="phase.list" /></p>

<security:authorize access="hasRole('HANDYWORKER')">

<!--Tabla-->

<display:table name="phases" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de borrar y editar en cada fila-->

   
    <security:authorize access="hasRole('HANDYWORKER')">
       <display:column titleKey="phase.delete">
        <a href="phase/handyWorker/delete.do?phaseId=${row.id}">
            <spring:message code="phase.delete"/>
        </a>
       </display:column>
       <display:column titleKey="phase.edit">
       <a href="phase/handyWorker/edit.do?phaseId=${row.id}">
           <spring:message code="phase.edit"/>
       </a>
       </display:column>
    </security:authorize>
        
	<spring:message code="phase.title" var="titleHeader"/>
	<display:column property="title" titleKey="phase.title" sortable="true"/>

	<spring:message code="phase.description" var="descriptionHeader"/>
	<display:column property="description" titleKey="phase.description" sortable="true"/>

	<spring:message code="phase.startMoment" var="startMomentHeader"/>
	<display:column property="startMoment" titleKey="phase.startMoment" sortable="true"/>

	<spring:message code="phase.endMoment" var="endMomentHeader"/>
	<display:column property="endMoment" titleKey="phase.endMoment" sortable="true"/>

</display:table>

</security:authorize>
