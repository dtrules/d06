<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="finder.list" /></p>

<security:authorize access="hasRole('ADMIN')">

<!--Tabla-->

<display:table name="categories" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar y mostrar en cada fila-->

	<security:authorize access="hasRole('ADMIN')">
	<display:column>
		<a href="finder/edit.do?finderId=${row.id}">
			<spring:message code="finder.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="finder/display.do?finderId=${row.id}">
			<spring:message code="finder.display"/>
		</a>
	</display:column>
	</security:authorize>
	<display:column property="name" titleKey="finder.name" sortable="true"/>
</display:table>

</security:authorize>