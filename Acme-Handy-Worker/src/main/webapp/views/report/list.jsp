<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!--Tabla-->

<display:table name="reports" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->


<security:authorize access="hasRole('REFEREE')">
	<display:column>
		<a href="report/referee/edit.do?reportId=${row.id}">
			<spring:message code="report.edit"/>
		</a>
	</display:column>
	</security:authorize>
	
	
	<display:column>
		<a href="report/display.do?reportId=${row.id}">
			<spring:message code="report.display"/>
		</a>
	</display:column>
	
	
	
	<spring:message code="report.moment" var="momentHeader"/>
	<display:column property="moment" title="${momentHeader}" sortable="true" />
	
	<spring:message code="report.complaint" var="complaintHeader"/>
	<display:column property="complaint.ticker" title="${complaintHeader}" sortable="true" />
	
	
	
	
</display:table>


<input type="button" name="cancel" onclick="javascript: window.location.replace('welcome/index.do')"
			value="<spring:message code="report.back" />" />


	
