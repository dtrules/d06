<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!--Tabla-->

<display:table name="sections" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->



	<display:column>
		<a href="section/display.do?sectionId=${row.id}">
			<spring:message code="section.display"/>
		</a>
	</display:column>
	
	
	
	<spring:message code="section.title" var="titleHeader"/>
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="section.text" var="textHeader"/>
	<display:column property="text" title="${textHeader}" sortable="true" />
	
	<spring:message code="section.picture" var="pictureHeader"/>
	<display:column property="picture" title="${pictureHeader}" sortable="true" />
	
	<spring:message code="section.number" var="numberHeader"/>
	<display:column property="number" title="${numberHeader}" sortable="true" />
	
	
	
	
</display:table>






	
