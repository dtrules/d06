
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.HandyWorker;

@Repository
public interface HandyWorkerRepository extends JpaRepository<HandyWorker, Integer> {

	@Query("select hw from HandyWorker hw join hw.applications a where a.status = 'ACCEPTED' and hw.applications.size >= (select avg(a2) from Application a2 where a2.status ="
		+ " 'ACCEPTED')+((select avg(a3) from Application a3 where a3.status = 'ACCEPTED')*0.1) order by hw.applications.size")
	Collection<HandyWorker> findHandyWorkerByAverageApplicationsAccepted();

	@Query("select h from HandyWorker h join h.applications a join a.task t where a.status='ACCEPTED' order by t.complaints.size")
	Collection<HandyWorker> findHandyWorkerByAcceptedApplicationsOrderedByComplaint();

	@Query("select h from HandyWorker h join h.applications a join a.task t where a.status='ACCEPTED' order by t.complaints.size")
	Collection<HandyWorker> top3HandyWorkerTermsOfComplaints();

	@Query("select c from Customer c where c.userAccount = ?1")
	HandyWorker findByUserAccount(UserAccount userAccount);

	@Query("select s from HandyWorker s where s.userAccount.username = ?1")
	HandyWorker findHandyWorkerByUsername(String username);

}
