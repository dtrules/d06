
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.CreditCard;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Integer> {

	@Query("select c from CreditCard c where c.holderName = ?1")
	CreditCard findCreditCardByHolderName(String holderName);

	@Query("select c from CreditCard c where c.number = ?1")
	CreditCard findCreditCardByNumber(long number);

}
