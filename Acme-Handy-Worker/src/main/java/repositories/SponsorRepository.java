
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Sponsor;

@Repository
public interface SponsorRepository extends JpaRepository<Sponsor, Integer> {

	@Query("select s from Sponsor s where s.userAccount.username = ?1")
	Sponsor findSponsorByUsername(String username);

	@Query("select s from Sponsor s where s.userAccount = ?1")
	Sponsor findByUserAccount(UserAccount userAccount);

}
