
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

	@Query("select min(r.notes.size), max(r.notes.size), avg(r.notes.size), stddev(r.notes.size) from Report r")
	Collection<Double> avgNotePerRefereeReport(Report r);

	@Query("select r from Report r join r.notes n where n.id = ?1")
	Report findReportByNote(int noteId);

	@Query("select r from Report r where r.draftMode=false")
	Collection<Report> findReportsByFinalMode();

}
