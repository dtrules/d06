
package repositories;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Finder;
import domain.Task;

@Repository
public interface FinderRepository extends JpaRepository<Finder, Integer> {

	@Query("select t from Task t where t.ticker = ?1")
	Collection<Task> findTasksByKeyWordTicker(String keyWord);

	@Query("select t from Task t where t.description = ?1")
	Collection<Task> findTasksByKeyWordDescription(String keyWord);

	@Query("select t from Task t where t.address = ?1")
	Collection<Task> findTasksByKeyWordAddress(String keyWord);

	@Query("select t from Task t where t.maxPrice < ?1")
	Collection<Task> findTasksByMaxPrice(Double maxPrice);

	@Query("select t from Task t where t.maxPrice > ?1")
	Collection<Task> findTasksByMinPrice(Double minPrice);

	@Query("select t from Task t where t.startMoment > ?1")
	Collection<Task> findTasksByStartDate(Date startDate);

	@Query("select t from Task t where t.endMoment > ?1")
	Collection<Task> findTasksByEndDate(Date endDate);

	@Query("select t from Task t where t.category.name = ?1")
	Collection<Task> findTasksByCategory(String category);

	@Query("select t from Task t where t.warranty.title = ?1")
	Collection<Task> findTasksByWarranty(String warranty);

}
