
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Finder;
import domain.Referee;

@Repository
public interface RefereeRepository extends JpaRepository<Referee, Integer> {

	@Query("select r from Referee r where r.userAccount.username = ?1")
	Referee findRefereeByUsername(String username);

	@Query("select r from Referee r where r.userAccount = ?1")
	Referee findByUserAccount(UserAccount userAccount);

	@Query("select r from Referee r join r.reports re where re.id=?1")
	Referee findRefereeByReportId(int reportId);

	@Query("select f from Finder f join f.tasks t where t.id = ?1")
	Collection<Finder> finderByTask(int taskId);

}
