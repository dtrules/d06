
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer> {

	@Query("select r.notes from Report r join r.complaint co where co.id=?1")
	Collection<Note> findNotesByComplaintId(int complaintId);

	@Query("select min(r.notes.size) from Report r")
	Double minNotesPerReport();

	@Query("select max(r.notes.size) from Report r")
	Double maxNotesPerReport();

	@Query("select avg(r.notes.size) from Report r")
	Double avgNotesPerReport();

	@Query("select stddev(r.notes.size) from Report r")
	Double deviationNotesPerReport();

}
