
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.CustomerEndorsement;

@Repository
public interface CustomerEndorsementRepository extends JpaRepository<CustomerEndorsement, Integer> {

	@Query("select ce from CustomerEndorsement ce where ce.handyWorker.id = ?1")
	Collection<CustomerEndorsement> getEndorsementsFromHandyWorker(final int handyWorkerId);
}
