package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.HandyWorkerEndorsement;

@Component
@Transactional
public class HandyWorkerEndorsementToStringConverter implements Converter<HandyWorkerEndorsement, String>{

	public String convert(final HandyWorkerEndorsement handyWorkerEndorsement) {
		String result;

		if (handyWorkerEndorsement == null)
			result = null;
		else
			result = String.valueOf(handyWorkerEndorsement.getId());

		return result;
	}
}
