package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.CustomerEndorsement;

@Component
@Transactional
public class CustomerEndorsementToStringConverter implements Converter<CustomerEndorsement, String>{

	public String convert(final CustomerEndorsement customerEndorsement) {
		String result;

		if (customerEndorsement == null)
			result = null;
		else
			result = String.valueOf(customerEndorsement.getId());

		return result;
	}
}
