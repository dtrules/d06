package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.HandyWorkerEndorsementRepository;

import domain.HandyWorkerEndorsement;

@Component
@Transactional
public class StringToHandyWorkerEndorsementConverter implements Converter<String, HandyWorkerEndorsement>{

	@Autowired
	HandyWorkerEndorsementRepository	handyWorkerEndorsementRepository;

	public HandyWorkerEndorsement convert(final String text) {
		HandyWorkerEndorsement result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.handyWorkerEndorsementRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}
}
