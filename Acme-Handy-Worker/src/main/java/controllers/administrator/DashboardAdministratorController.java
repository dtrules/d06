
package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ApplicationService;
import services.CustomerService;
import services.HandyWorkerService;
import services.NoteService;
import services.TaskService;
import controllers.AbstractController;
import domain.Customer;
import domain.HandyWorker;

@Controller
@RequestMapping("dashboard/administrator")
public class DashboardAdministratorController extends AbstractController {

	//Services---------------------------------------------------------------------------------------
	@Autowired
	private TaskService			taskService;

	@Autowired
	private CustomerService		customerService;

	@Autowired
	private HandyWorkerService	handyWorkerService;

	@Autowired
	private ApplicationService	applicationService;

	@Autowired
	private NoteService			noteService;


	//Constructor --------------------------
	public DashboardAdministratorController() {
		super();
	}

	//Dashboard
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final ModelAndView result;

		//1
		final Double minNumberOfComplaints = this.taskService.minNumberOfComplaints();
		final Double maxNumberOfComplaints = this.taskService.maxNumberOfComplaints();
		final Double avgNumberOfComplaints = this.taskService.avgNumberOfComplaints();
		final Double deviationNumberOfComplaints = this.taskService.deviationNumberOfComplaints();

		//2
		final Double minNotesPerReport = this.noteService.minNotesPerReferee();
		final Double maxNotesPerReport = this.noteService.maxNotesPerReferee();
		final Double avgNotesPerReport = this.noteService.avgNotesPerReferee();
		final Double deviationNotesPerReport = this.noteService.deviationNotesPerReferee();

		//3
		final Double minTasksPerUser = this.customerService.minTasksPerUser();
		final Double maxTasksPerUser = this.customerService.maxTasksPerUser();
		final Double avgTasksPerUser = this.customerService.avgTasksPerUser();
		final Double deviationTasksPerUser = this.customerService.deviationTasksPerUser();

		//4
		final Double minApplicationsPerTask = this.taskService.minNumberOfApplications();
		final Double maxApplicationsPerTask = this.taskService.maxNumberOfApplications();
		final Double avgApplicationsPerTask = this.taskService.avgNumberOfApplications();
		final Double deviationApplicationsPerTask = this.taskService.deviationNumberOfApplications();

		//5
		final Double minMaxPricePerTask = this.taskService.minMaxPricePerTask();
		final Double maxMaxPricePerTask = this.taskService.maxMaxPricePerTask();
		final Double avgMaxPricePerTask = this.taskService.avgMaxPricePerTask();
		final Double deviationMaxPricePerTask = this.taskService.deviationMaxPricePerTask();

		//6
		final Double minPricePerApplication = this.applicationService.minPricePerApplication();
		final Double maxPricePerApplication = this.applicationService.maxPricePerApplication();
		final Double avgPricePerApplication = this.applicationService.avgPricePerApplication();
		final Double deviationPricePerApplication = this.applicationService.deviationPricePerApplication();

		//7
		final Double ratioOfTasksWithAComplaint = this.taskService.ratioOfTasksWithAComplaint();

		//8
		final Double ratioOfPendingApplications = this.applicationService.ratioOfPendingApplications();

		//9
		final Double ratioOfAcceptedApplications = this.applicationService.ratioOfAcceptedApplications();

		//10
		final Double ratioOfRejectedApplications = this.applicationService.ratioOfRejectedApplications();

		//11
		final Double ratioOfElapsedPendingApplications = this.applicationService.ratioOfPendingApplicationsElapsed();

		//12
		final Collection<Customer> customerListPublishedTasks = this.customerService.customerListPublishedTasks();

		//13
		final Collection<HandyWorker> handyWorkerListByAcceptedApplications = this.handyWorkerService.findHandyWorkerByAcceptedApplicationsOrderedByComplaint();

		//14
		final Collection<Customer> top3CustomerByComplaint = this.customerService.top3CustomerByComplaint();

		//15
		final Collection<HandyWorker> top3HandyWorkerByComplaint = this.handyWorkerService.top3HandyWorkerTermsOfComplaints();

		result = new ModelAndView("dashboard/administrator/list");

		result.addObject("minNumberOfComplaints", minNumberOfComplaints);
		result.addObject("maxNumberOfComplaints", maxNumberOfComplaints);
		result.addObject("avgNumberOfComplaints", avgNumberOfComplaints);
		result.addObject("deviationNumberOfComplaints", deviationNumberOfComplaints);

		result.addObject("minNotesPerReport", minNotesPerReport);
		result.addObject("maxNotesPerReport", maxNotesPerReport);
		result.addObject("avgNotesPerReport", avgNotesPerReport);
		result.addObject("deviationNotesPerReport", deviationNotesPerReport);

		result.addObject("minTasksPerUser", minTasksPerUser);
		result.addObject("maxTasksPerUser", maxTasksPerUser);
		result.addObject("avgTasksPerUser", avgTasksPerUser);
		result.addObject("deviationTasksPerUser", deviationTasksPerUser);

		result.addObject("minApplicationsPerTask", minApplicationsPerTask);
		result.addObject("maxApplicationsPerTask", maxApplicationsPerTask);
		result.addObject("avgApplicationsPerTask", avgApplicationsPerTask);
		result.addObject("deviationApplicationsPerTask", deviationApplicationsPerTask);

		result.addObject("minMaxPricePerTask", minMaxPricePerTask);
		result.addObject("maxMaxPricePerTask", maxMaxPricePerTask);
		result.addObject("avgMaxPricePerTask", avgMaxPricePerTask);
		result.addObject("deviationMaxPricePerTask", deviationMaxPricePerTask);

		result.addObject("minPricePerApplication", minPricePerApplication);
		result.addObject("maxPricePerApplication", maxPricePerApplication);
		result.addObject("avgPricePerApplication", avgPricePerApplication);
		result.addObject("deviationPricePerApplication", deviationPricePerApplication);

		result.addObject("ratioOfTasksWithAComplaint", ratioOfTasksWithAComplaint);

		result.addObject("ratioOfPendingApplications", ratioOfPendingApplications);

		result.addObject("ratioOfAcceptedApplications", ratioOfAcceptedApplications);

		result.addObject("ratioOfRejectedApplications", ratioOfRejectedApplications);

		result.addObject("ratioOfElapsedPendingApplications", ratioOfElapsedPendingApplications);

		result.addObject("customerListPublishedTasks", customerListPublishedTasks);

		result.addObject("handyWorkerListByAcceptedApplications", handyWorkerListByAcceptedApplications);

		result.addObject("top3CustomerByComplaint", top3CustomerByComplaint);

		result.addObject("top3HandyWorkerByComplaint", top3HandyWorkerByComplaint);

		return result;
	}
}
