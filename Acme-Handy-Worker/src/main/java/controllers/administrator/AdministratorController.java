
package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ActorService			actorService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Administrator administrator = this.administratorService.create();

		final ModelAndView result = this.createEditModelAndView(administrator);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Administrator administrator, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(administrator);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.administratorService.save(administrator);

				result = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				result = this.createEditModelAndView(administrator, "administrator.commit.error");
			}

		return result;
	}

	// List suspicious ----------------------------------------------------------

	@RequestMapping(value = "/suspicious", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;

		final Collection<Actor> suspiciousActors = this.administratorService.findSuspicious();

		res = new ModelAndView("administrator/suspicious");
		res.addObject("requestURI", "administrator/suspicious.do");
		res.addObject("suspiciousActors", suspiciousActors);

		return res;
	}

	//Ban/unban --------------------------------------------------------------------

	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public void ban(@RequestParam final Integer actorId) {

		final Actor banActor = this.actorService.findOne(actorId);
		this.administratorService.ban(banActor);

	}

	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public void unban(@RequestParam final Integer actorId) {

		final Actor unbanActor = this.actorService.findOne(actorId);
		this.administratorService.unban(unbanActor);

	}

	//Ancillary methods ------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Administrator administrator) {

		ModelAndView result;
		result = this.createEditModelAndView(administrator, null);
		return result;

	}

	protected ModelAndView createEditModelAndView(final Administrator administrator, final String message) {

		ModelAndView result;
		result = new ModelAndView("administrator/create");
		result.addObject("administrator", administrator);
		result.addObject("message", message);
		return result;
	}

}
