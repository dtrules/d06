/*
 * AdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.WarrantyService;
import controllers.AbstractController;
import domain.Warranty;

@Controller
@RequestMapping("/warranty/administrator")
public class WarrantyAdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public WarrantyAdministratorController() {
		super();
	}


	// Services ---------------------------------------------------------------
	@Autowired
	private WarrantyService	warrantyService;


	// List ---------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {

		final Collection<Warranty> warranties;

		warranties = this.warrantyService.findAll();

		final ModelAndView result = new ModelAndView("warranty/administrator/list");
		result.addObject("warranties", warranties);
		result.addObject("message", message);
		result.addObject("requestURI", "warranty/administrator/list.do");

		return result;
	}

	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Warranty warranty = this.warrantyService.create();

		final ModelAndView result = this.createEditModelAndView(warranty);

		return result;
	}

	// Save del create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Warranty warranty, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(warranty);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.warrantyService.save(warranty);

				result = new ModelAndView("redirect:/warranty/administrator/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(warranty, "warranty.commit.error");
			}

		return result;
	}

	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int warrantyId) {
		ModelAndView result;
		Warranty warranty;

		warranty = this.warrantyService.findOne(warrantyId);
		result = this.createEditModelAndView(warranty);

		return result;
	}

	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Warranty warranty, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(warranty);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.warrantyService.save(warranty);
				result = new ModelAndView("redirect:/warranty/administrator/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(warranty, "warranty.commit.error");
			}

		return result;
	}

	// Delete ---------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int warrantyId) {
		ModelAndView result;

		final Warranty warranty = this.warrantyService.findOne(warrantyId);

		try {

			this.warrantyService.delete(warranty);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/warranty/administrator/list.do");

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Warranty warranty) {

		return this.createEditModelAndView(warranty, null);
	}

	private ModelAndView createEditModelAndView(final Warranty warranty, final String message) {

		final ModelAndView res = new ModelAndView("warranty/administrator/edit");

		res.addObject("warranty", warranty);
		res.addObject("message", message);

		return res;

	}

}
