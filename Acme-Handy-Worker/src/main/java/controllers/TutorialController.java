
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.HandyWorkerService;
import services.SponsorshipService;
import services.TutorialService;
import domain.HandyWorker;
import domain.Sponsorship;
import domain.Tutorial;

@Controller
@RequestMapping("/tutorial")
public class TutorialController extends AbstractController {

	//Constructors ----------------------

	public TutorialController() {
		super();
	}


	//Services -----------------------------

	@Autowired
	private TutorialService		tutorialService;

	@Autowired
	private SponsorshipService	sponsorshipService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		Collection<Tutorial> tutorials = new ArrayList<Tutorial>();
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		if (handyWorker != null)
			tutorials = handyWorker.getTutorials();
		else
			tutorials = this.tutorialService.findAll();

		final ModelAndView result = new ModelAndView("tutorial/list");
		result.addObject("tutorials", tutorials);
		result.addObject("requestURI", "tutorials/list.do");

		return result;
	}

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer tutorialId) {
		ModelAndView result;
		Tutorial tutorial;
		Sponsorship sponsorship;

		tutorial = this.tutorialService.findOne(tutorialId);

		sponsorship = this.sponsorshipService.findRandomOneByTutorial(tutorialId);
		result = new ModelAndView("tutorial/display");
		result.addObject("tutorial", tutorial);
		result.addObject("sponsorship", sponsorship);
		result.addObject("tutorialId", tutorial.getId());

		return result;
	}

}
