
package controllers.referee;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ComplaintService;
import services.RefereeService;
import services.ReportService;
import controllers.AbstractController;
import domain.Complaint;
import domain.Referee;
import domain.Report;

@Controller
@RequestMapping("/report/referee")
public class ReportRefereeController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public ReportRefereeController() {
		super();
	}


	// Services ---------------------------------------------------------------
	@Autowired
	private ReportService		reportService;

	@Autowired
	private ComplaintService	complaintService;

	@Autowired
	private RefereeService		refereeService;


	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) final int complaintId) {
		final Complaint complaint = this.complaintService.findOne(complaintId);
		final Report report = this.reportService.create();

		final ModelAndView result = this.createModelAndView(report);
		result.addObject("complaintId", complaintId);

		return result;
	}

	// Save del create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@RequestParam(required = true) final int complaintId, @Valid final Report report, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createModelAndView(report);
		//System.out.println(binding.getAllErrors());
		else
			try {
				final Complaint complaint = this.complaintService.findOne(complaintId);
				this.reportService.save(report, complaint);
				result = new ModelAndView("redirect:/complaint/referee/listAssigned.do");
				result.addObject("complaintId", complaintId);

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createModelAndView(report, "report.commit.error");
			}

		return result;
	}
	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int reportId) {
		ModelAndView result;
		Report report;

		report = this.reportService.findOne(reportId);
		result = this.editModelAndView(report);
		result.addObject("reportId", reportId);
		result.addObject("complaint", report.getComplaint());

		return result;
	}

	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Report report, final BindingResult binding) {
		ModelAndView result;

		//		if (binding.hasErrors())
		result = this.editModelAndView(report);
		//System.out.println(binding.getAllErrors());
		//		else
		//			try {
		this.reportService.saveEdit(report);
		//		result = new ModelAndView("redirect:/task/list.do");

		//			} catch (final Throwable oops) {
		//				//System.out.println(oops);
		//				result = this.editModelAndView(report, "report.commit.error");
		//			}

		return result;
	}

	// Delete ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView deleteEdit(@Valid final Report report, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.editModelAndView(report);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.reportService.delete(report);
				result = new ModelAndView("redirect:/task/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.editModelAndView(report, "report.commit.error");
			}

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		Collection<Report> reports = new ArrayList<Report>();
		final Referee referee = this.refereeService.findByPrincipal();
		reports = referee.getReports();

		final ModelAndView result = new ModelAndView("report/referee/list");
		result.addObject("reports", reports);
		result.addObject("requestURI", "report/referee/list.do");

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createModelAndView(final Report report) {

		return this.createModelAndView(report, null);
	}

	private ModelAndView createModelAndView(final Report report, final String message) {

		final ModelAndView res = new ModelAndView("report/referee/create");
		res.addObject("report", report);
		res.addObject("message", message);

		return res;

	}

	private ModelAndView editModelAndView(final Report report) {

		return this.editModelAndView(report, null);
	}

	private ModelAndView editModelAndView(final Report report, final String message) {

		final ModelAndView res = new ModelAndView("report/referee/edit");
		res.addObject("report", report);
		res.addObject("message", message);

		return res;

	}
}
