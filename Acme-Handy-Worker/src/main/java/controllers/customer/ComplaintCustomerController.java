
package controllers.customer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ComplaintService;
import services.CustomerService;
import services.TaskService;
import controllers.AbstractController;
import domain.Complaint;
import domain.Customer;
import domain.Task;

@Controller
@RequestMapping("/complaint/customer")
public class ComplaintCustomerController extends AbstractController {

	//Constructors ----------------------

	public ComplaintCustomerController() {
		super();
	}


	//Services ---------------------
	@Autowired
	private ComplaintService	complaintService;

	@Autowired
	private CustomerService		customerService;

	@Autowired
	private TaskService			taskService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) final Integer taskId) {

		final Complaint complaint = this.complaintService.create();

		final ModelAndView result = this.createEditModelAndView(complaint);
		result.addObject("taskId", taskId);

		return result;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@RequestParam(required = true) final Integer taskId, @Valid final Complaint complaint, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(complaint);
		else
			try {
				final Task task = this.taskService.findOne(taskId);

				this.complaintService.save(complaint, task);

				result = new ModelAndView("redirect:/task/list.do");
				result.addObject("taskId", taskId);

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(complaint, "complaint.commit.error");
			}
		return result;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int complaintId) {
		ModelAndView result;
		Complaint complaint;

		complaint = this.complaintService.findOne(complaintId);
		result = this.createEditModelAndView(complaint);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final Customer customer = this.customerService.findByPrincipal();
		final Collection<Complaint> complaints = this.complaintService.findComplaintByCustomer(customer.getId());

		final ModelAndView result = new ModelAndView("complaint/customer/list");
		result.addObject("complaints", complaints);
		result.addObject("requestURI", "complaint/customer/list.do");

		return result;
	}

	@RequestMapping(value = "/listByTask", method = RequestMethod.GET)
	public ModelAndView listComplaintsByTask(@RequestParam final int taskId) {
		final Collection<Complaint> complaints = this.complaintService.findComplaintsByTask(taskId);

		final ModelAndView result = new ModelAndView("complaint/customer/list");
		result.addObject("complaints", complaints);
		result.addObject("requestURI", "complaint/customer/listByTask.do");

		return result;
	}

	private ModelAndView createEditModelAndView(final Complaint complaint) {

		return this.createEditModelAndView(complaint, null);
	}

	private ModelAndView createEditModelAndView(final Complaint complaint, final String message) {

		final ModelAndView res = new ModelAndView("complaint/customer/create");
		res.addObject("complaint", complaint);
		res.addObject("message", message);

		return res;

	}

}
