package controllers.customer;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CustomerEndorsementService;
import services.CustomerService;
import domain.CustomerEndorsement;

@Controller
@RequestMapping("/customerEndorsement/customer")
public class CustomerEndorsementCustomerController extends AbstractController{

	CustomerEndorsementCustomerController(){
		super();
	}

	// Services

	@Autowired
	CustomerEndorsementService customerEndorsementService;
	@Autowired
	CustomerService customerService;

	// Display ---------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer customerEndorsementId) {
		ModelAndView result;
		CustomerEndorsement customerEndorsement;

		customerEndorsement = this.customerEndorsementService.findOne(customerEndorsementId);

		result = new ModelAndView("customerEndorsement/display");
		result.addObject("customerEndorsement", customerEndorsement);

		return result;
	}

	// Create ----------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final CustomerEndorsement customerEndorsement = this.customerEndorsementService.create();

		final ModelAndView result = this.createEditModelAndView(customerEndorsement);

		return result;
	}

	// Save del create -------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final CustomerEndorsement customerEndorsement,
			final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(customerEndorsement);
			// System.out.println(binding.getAllErrors());
		} else
			try {
				this.customerEndorsementService.save(customerEndorsement);

				res = new ModelAndView("redirect:/customerEndorsement/list.do");

			} catch (final Throwable oops) {
				// System.out.println(oops);
				res = this.createEditModelAndView(customerEndorsement,
						"customerEndorsement.commit.error");
			}

		return res;
	}

	// Edit ------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int customerEndorsementId) {
		ModelAndView res;
		CustomerEndorsement customerEndorsement;

		customerEndorsement = this.customerEndorsementService.findOne(customerEndorsementId);
		Assert.notNull(customerEndorsement);
		res = createEditModelAndView(customerEndorsement);

		return res;
	}

	// Save del Edit----------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final CustomerEndorsement customerEndorsement,
			final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(customerEndorsement);
		} else
			try {
				this.customerEndorsementService.save(customerEndorsement);
				res = new ModelAndView("redirect:/customerEndorsement/list.do");

			} catch (final Throwable oops) {
				res = this.createEditModelAndView(customerEndorsement,
						"customerEndorsement.commit.error");
			}

		return res;
	}

	// List ----------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<CustomerEndorsement> customerEndorsements;

		if (this.customerService.findByPrincipal() != null)
			customerEndorsements = this.customerService.findByPrincipal().getCustomerEndorsements();
		else
			customerEndorsements = this.customerEndorsementService.findAll();
		
		res = new ModelAndView("customerEndorsement/list");
		res.addObject("requestURI", "customerEndorsement/customer/list.do");
		res.addObject("customerEndorsements", customerEndorsements);

		return res;
	}

	// Ancillary methods
	// ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final CustomerEndorsement customerEndorsement) {

		return this.createEditModelAndView(customerEndorsement, null);
	}

	private ModelAndView createEditModelAndView(final CustomerEndorsement customerEndorsement,
			final String message) {

		final ModelAndView res = new ModelAndView("customerEndorsement/edit");
		res.addObject("customer", customerEndorsement);
		res.addObject("message", message);

		return res;

	}
}
