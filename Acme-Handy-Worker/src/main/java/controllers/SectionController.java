
package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SectionService;
import services.TutorialService;
import domain.Section;
import domain.Tutorial;

@Controller
@RequestMapping("/section")
public class SectionController extends AbstractController {

	//Constructors ----------------------

	public SectionController() {
		super();
	}


	//Services -----------------------------

	@Autowired
	private SectionService	sectionService;

	@Autowired
	private TutorialService	tutorialService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam final Integer tutorialId) {
		final Collection<Section> sections;
		final Tutorial tutorial = this.tutorialService.findOne(tutorialId);
		sections = tutorial.getSections();

		final ModelAndView result = new ModelAndView("section/list");
		result.addObject("sections", sections);
		result.addObject("requestURI", "section/list.do");

		return result;
	}
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer sectionId) {
		ModelAndView result;
		Section section;

		section = this.sectionService.findOne(sectionId);

		result = new ModelAndView("section/display");
		result.addObject("section", section);

		return result;
	}

}
