
package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.HandyWorkerService;
import services.MiscellaneousRecordService;
import controllers.AbstractController;
import domain.MiscellaneousRecord;

@Controller
@RequestMapping("/miscellaneousRecord/handyWorker")
public class MiscellaneousRecordHandyWorkerController extends AbstractController {

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;

	@Autowired
	private HandyWorkerService			handyWorkerService;


	// Constructors -----------------------------------------------------------

	public MiscellaneousRecordHandyWorkerController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<MiscellaneousRecord> miscellaneousRecords;

		if (this.handyWorkerService.findByPrincipal() != null)
			miscellaneousRecords = this.handyWorkerService.findByPrincipal().getCurriculum().getMiscellaneousRecords();
		else
			miscellaneousRecords = this.miscellaneousRecordService.findAll();

		result = new ModelAndView("miscellaneousRecord/list");
		result.addObject("requestURI", "miscellaneousRecord/handyWorker/list.do");
		result.addObject("miscellaneousRecords", miscellaneousRecords);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		MiscellaneousRecord miscellaneousRecord;

		miscellaneousRecord = this.miscellaneousRecordService.create();
		this.handyWorkerService.findByPrincipal().getCurriculum().getMiscellaneousRecords().add(miscellaneousRecord);
		result = this.createEditModelAndView(miscellaneousRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int miscellaneousRecordId) {
		ModelAndView result;
		MiscellaneousRecord miscellaneousRecord;

		miscellaneousRecord = this.miscellaneousRecordService.findOne(miscellaneousRecordId);
		result = this.createEditModelAndView(miscellaneousRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final MiscellaneousRecord miscellaneousRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(miscellaneousRecord);
		else
			try {
				this.miscellaneousRecordService.save(miscellaneousRecord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(miscellaneousRecord, "miscellaneousRecord.commit.error");
			}

		return result;
	}

	protected ModelAndView createEditModelAndView(final MiscellaneousRecord miscellaneousRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(miscellaneousRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final MiscellaneousRecord miscellaneousRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("miscellaneousRecord/edit");

		result.addObject("message", message);
		result.addObject("miscellaneousRecord", miscellaneousRecord);

		return result;

	}

	// Delete ---------------------------------------------------------------
	//
	//	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	//	public ModelAndView delete(@RequestParam final int educationRecordId) {
	//		ModelAndView result;
	//
	//		final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordId);
	//
	//		try {
	//
	//			this.educationRecordService.delete(educationRecord);
	//		} catch (final Throwable th) {
	//
	//		}
	//
	//		result = new ModelAndView("redirect:/educationRecord/handyWorker/list.do");
	//
	//		return result;
	//	}

}
