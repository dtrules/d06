
package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.HandyWorkerService;
import services.ProfessionalRecordService;
import controllers.AbstractController;
import domain.ProfessionalRecord;

@Controller
@RequestMapping("/professionalRecord/handyWorker")
public class ProfessionalRecordHandyWorkerController extends AbstractController {

	@Autowired
	private ProfessionalRecordService	professionalRecordService;

	@Autowired
	private HandyWorkerService			handyWorkerService;


	// Constructors -----------------------------------------------------------

	public ProfessionalRecordHandyWorkerController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<ProfessionalRecord> professionalRecords;

		if (this.handyWorkerService.findByPrincipal() != null)
			professionalRecords = this.handyWorkerService.findByPrincipal().getCurriculum().getProfessionalRecords();
		else
			professionalRecords = this.professionalRecordService.findAll();

		result = new ModelAndView("professionalRecord/list");
		result.addObject("requestURI", "professionalRecord/handyWorker/list.do");
		result.addObject("professionalRecords", professionalRecords);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		ProfessionalRecord professionalRecord;

		professionalRecord = this.professionalRecordService.create();
		this.handyWorkerService.findByPrincipal().getCurriculum().getProfessionalRecords().add(professionalRecord);
		result = this.createEditModelAndView(professionalRecord);

		return result;
	}

	protected ModelAndView createEditModelAndView(final ProfessionalRecord professionalRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(professionalRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final ProfessionalRecord professionalRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("professionalRecord/edit");

		result.addObject("message", message);
		result.addObject("professionalRecord", professionalRecord);

		return result;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ProfessionalRecord professionalRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(professionalRecord);
		else
			try {
				this.professionalRecordService.save(professionalRecord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(professionalRecord, "professionalRecord.commit.error");
			}

		return result;
	}

}
