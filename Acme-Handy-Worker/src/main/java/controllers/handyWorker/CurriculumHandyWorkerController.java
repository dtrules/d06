
package controllers.handyWorker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CurriculumService;
import services.HandyWorkerService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.HandyWorker;

@Controller
@RequestMapping("/curriculum/handyWorker")
public class CurriculumHandyWorkerController extends AbstractController {

	@Autowired
	private CurriculumService	curriculumService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	// Constructors -----------------------------------------------------------

	public CurriculumHandyWorkerController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {
		ModelAndView result;
		boolean haveCurriculum = false;
		String ticker = "";

		if (this.handyWorkerService.findByPrincipal().getCurriculum() != null) {
			haveCurriculum = true;
			ticker = this.handyWorkerService.findByPrincipal().getCurriculum().getTicker();
		}
		result = new ModelAndView("curriculum/list");
		result.addObject("message", message);
		result.addObject("ticker", ticker);
		result.addObject("haveCurriculum", haveCurriculum);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;

		final Curriculum curriculum = this.curriculumService.create();

		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		handyWorker.setCurriculum(curriculum);
		this.handyWorkerService.save(handyWorker);

		result = new ModelAndView("redirect:/curriculum/handyWorker/list.do");

		return result;
	}

}
