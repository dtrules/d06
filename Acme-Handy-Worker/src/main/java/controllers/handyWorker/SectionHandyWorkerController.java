
package controllers.handyWorker;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SectionService;
import services.TutorialService;
import controllers.AbstractController;
import domain.Section;
import domain.Tutorial;

@Controller
@RequestMapping("/section/handyWorker")
public class SectionHandyWorkerController extends AbstractController {

	//Constructor --------------------------

	public SectionHandyWorkerController() {
		super();
	}


	//Services ----------------------

	@Autowired
	private SectionService	sectionService;

	@Autowired
	private TutorialService	tutorialService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) final Integer tutorialId) {

		final Section section = this.sectionService.create();

		final ModelAndView result = this.createEditModelAndView(section);
		result.addObject("tutorialId", tutorialId);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@RequestParam(required = true) final Integer tutorialId, @Valid final Section section, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(section);
		//System.out.println(binding.getAllErrors());
		else
			try {
				final Tutorial tutorial = this.tutorialService.findOne(tutorialId);
				this.sectionService.save(section, tutorial);

				result = new ModelAndView("redirect:/tutorial/list.do");
				result.addObject("tutorialId", tutorialId);

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(section, "section.commit.error");
			}

		return result;
	}

	private ModelAndView createEditModelAndView(final Section section) {

		return this.createEditModelAndView(section, null);
	}

	private ModelAndView createEditModelAndView(final Section section, final String message) {
		final ModelAndView res = new ModelAndView("section/handyWorker/create");
		res.addObject("section", section);
		res.addObject("message", message);

		return res;

	}

}
