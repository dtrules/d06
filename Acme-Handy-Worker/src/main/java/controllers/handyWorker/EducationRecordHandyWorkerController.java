
package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EducationRecordService;
import services.HandyWorkerService;
import controllers.AbstractController;
import domain.EducationRecord;

@Controller
@RequestMapping("/educationRecord/handyWorker")
public class EducationRecordHandyWorkerController extends AbstractController {

	@Autowired
	private EducationRecordService	educationRecordService;

	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Constructors -----------------------------------------------------------

	public EducationRecordHandyWorkerController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {
		ModelAndView result;
		Collection<EducationRecord> educationRecords;

		if (this.handyWorkerService.findByPrincipal() != null)
			educationRecords = this.handyWorkerService.findByPrincipal().getCurriculum().getEducationRecords();
		else
			educationRecords = this.educationRecordService.findAll();

		result = new ModelAndView("educationRecord/list");
		result.addObject("requestURI", "educationRecord/handyWorker/list.do");
		result.addObject("message", message);
		result.addObject("educationRecord", educationRecords);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		EducationRecord educationRecord;

		educationRecord = this.educationRecordService.create();
		this.handyWorkerService.findByPrincipal().getCurriculum().getEducationRecords().add(educationRecord);
		result = this.createEditModelAndView(educationRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int educationRecordId) {
		ModelAndView result;
		EducationRecord educationRecord;

		educationRecord = this.educationRecordService.findOne(educationRecordId);
		result = this.createEditModelAndView(educationRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final EducationRecord educationRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(educationRecord);
		else
			try {
				this.educationRecordService.save(educationRecord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(educationRecord, "educationRecord.commit.error");
			}

		return result;
	}

	protected ModelAndView createEditModelAndView(final EducationRecord educationRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(educationRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final EducationRecord educationRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("educationRecord/edit");

		result.addObject("message", message);
		result.addObject("educationRecord", educationRecord);

		return result;

	}

	// Delete ---------------------------------------------------------------
	//
	//	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	//	public ModelAndView delete(@RequestParam final int educationRecordId) {
	//		ModelAndView result;
	//
	//		final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordId);
	//
	//		try {
	//
	//			this.educationRecordService.delete(educationRecord);
	//		} catch (final Throwable th) {
	//
	//		}
	//
	//		result = new ModelAndView("redirect:/educationRecord/handyWorker/list.do");
	//
	//		return result;
	//	}

}
