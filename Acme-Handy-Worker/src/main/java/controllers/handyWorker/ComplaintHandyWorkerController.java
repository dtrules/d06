
package controllers.handyWorker;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ComplaintService;
import services.HandyWorkerService;
import controllers.AbstractController;
import domain.Complaint;
import domain.HandyWorker;

@Controller
@RequestMapping("/complaint/handyWorker")
public class ComplaintHandyWorkerController extends AbstractController {

	//Constructors --------------------------------------

	public ComplaintHandyWorkerController() {
		super();

	}


	//Services ----------------------------------------------

	@Autowired
	private ComplaintService	complaintService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		final Collection<Complaint> complaints = this.handyWorkerService.findComplaintsByHandyWorker(handyWorker);
		final ModelAndView result = new ModelAndView("complaint/list");
		result.addObject("complaints", complaints);
		result.addObject("requestURI", "complaint/handyWorker/list.do");

		return result;
	}
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer complaintId) {
		ModelAndView result;
		Complaint complaint;

		complaint = this.complaintService.findOne(complaintId);

		result = new ModelAndView("complaint/display");
		result.addObject("complaint", complaint);

		return result;
	}

	private ModelAndView createEditModelAndView(final Complaint complaint) {

		return this.createEditModelAndView(complaint, null);
	}

	private ModelAndView createEditModelAndView(final Complaint complaint, final String message) {

		final ModelAndView res = new ModelAndView("complaint/handyWorker/list");
		res.addObject("complaint", complaint);
		res.addObject("message", message);

		return res;

	}

}
