package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.HandyWorkerEndorsementService;
import services.HandyWorkerService;

import controllers.AbstractController;
import domain.HandyWorkerEndorsement;

@Controller
@RequestMapping("/handyWorkerEndorsement/handyWorker")
public class HandyWorkerEndorsementHandyWorkerController extends
		AbstractController {

	HandyWorkerEndorsementHandyWorkerController() {
		super();
	}

	// Services

	@Autowired
	HandyWorkerEndorsementService handyWorkerEndorsementService;
	@Autowired
	HandyWorkerService handyWorkerService;

	// Display ---------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(
			@RequestParam final Integer handyWorkerEndorsementId) {
		ModelAndView result;
		HandyWorkerEndorsement handyWorkerEndorsement;

		handyWorkerEndorsement = this.handyWorkerEndorsementService
				.findOne(handyWorkerEndorsementId);

		result = new ModelAndView("handyWorkerEndorsement/display");
		result.addObject("handyWorkerEndorsement", handyWorkerEndorsement);

		return result;
	}

	// Create ----------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final HandyWorkerEndorsement handyWorkerEndorsement = this.handyWorkerEndorsementService
				.create();

		final ModelAndView result = this
				.createEditModelAndView(handyWorkerEndorsement);

		return result;
	}

	// Save del create -------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(
			@Valid final HandyWorkerEndorsement handyWorkerEndorsement,
			final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(handyWorkerEndorsement);
			// System.out.println(binding.getAllErrors());
		} else
			try {
				this.handyWorkerEndorsementService.save(handyWorkerEndorsement);

				res = new ModelAndView(
						"redirect:/handyWorkerEndorsement/list.do");

			} catch (final Throwable oops) {
				// System.out.println(oops);
				res = this.createEditModelAndView(handyWorkerEndorsement,
						"handyWorkerEndorsement.commit.error");
			}

		return res;
	}

	// Edit ------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int handyWorkerEndorsementId) {
		ModelAndView res;
		HandyWorkerEndorsement handyWorkerEndorsement;

		handyWorkerEndorsement = this.handyWorkerEndorsementService
				.findOne(handyWorkerEndorsementId);
		Assert.notNull(handyWorkerEndorsement);
		res = createEditModelAndView(handyWorkerEndorsement);

		return res;
	}

	// Save del Edit----------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(
			@Valid final HandyWorkerEndorsement handyWorkerEndorsement,
			final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(handyWorkerEndorsement);
		} else
			try {
				this.handyWorkerEndorsementService.save(handyWorkerEndorsement);
				res = new ModelAndView(
						"redirect:/handyWorkerEndorsement/list.do");

			} catch (final Throwable oops) {
				res = this.createEditModelAndView(handyWorkerEndorsement,
						"handyWorkerEndorsement.commit.error");
			}

		return res;
	}

	// List ----------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<HandyWorkerEndorsement> handyWorkerEndorsements;

		if (this.handyWorkerService.findByPrincipal() != null)
			handyWorkerEndorsements = this.handyWorkerService.findByPrincipal()
					.getHandyWorkerEndorsements();
		else
			handyWorkerEndorsements = this.handyWorkerEndorsementService
					.findAll();

		res = new ModelAndView("handyWorkerEndorsement/list");
		res.addObject("requestURI",
				"handyWorkerEndorsement/handyWorker/list.do");
		res.addObject("handyWorkerEndorsements", handyWorkerEndorsements);

		return res;
	}

	// Ancillary methods
	// ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(
			final HandyWorkerEndorsement handyWorkerEndorsement) {

		return this.createEditModelAndView(handyWorkerEndorsement, null);
	}

	private ModelAndView createEditModelAndView(
			final HandyWorkerEndorsement handyWorkerEndorsement,
			final String message) {

		final ModelAndView res = new ModelAndView("handyWorkerEndorsement/edit");
		res.addObject("handyWorker", handyWorkerEndorsement);
		res.addObject("message", message);

		return res;

	}
}
