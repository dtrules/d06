
package controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.NoteService;
import services.ReportService;
import domain.Note;
import domain.Report;

@Controller
@RequestMapping("/note")
public class NoteController extends AbstractController {

	//Constructor --------------------------

	public NoteController() {
		super();
	}


	//Services ----------------------

	@Autowired
	private NoteService		noteService;

	@Autowired
	private ReportService	reportService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) final Integer reportId) {

		final Report report = this.reportService.findOne(reportId);
		final Note note = this.noteService.create(report);

		final ModelAndView result = this.createEditModelAndView(note);
		result.addObject("reportId", reportId);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Note note, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(note);
		else
			try {
				this.noteService.save(note);
				System.out.println(binding);
				result = new ModelAndView("redirect;/report/list.do");

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(note, "note.comit.error");
			}
		return result;
	}

	private ModelAndView createEditModelAndView(final Note note) {

		return this.createEditModelAndView(note, null);
	}

	private ModelAndView createEditModelAndView(final Note note, final String message) {

		final ModelAndView res = new ModelAndView("note/edit");
		res.addObject("note", note);
		res.addObject("message", message);

		return res;

	}

}
