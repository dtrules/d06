
package controllers.actor;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.FolderService;
import services.MessageService;
import services.SponsorshipService;
import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Controller
@RequestMapping("/message/actor")
public class MessageActorController extends AbstractController {

	//Services ----------------------------------------------------------
	@Autowired
	MessageService		messageService;

	@Autowired
	ActorService		actorService;

	@Autowired
	SponsorshipService	sponsorshipService;

	@Autowired
	FolderService		folderService;


	//List----------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false) final Integer folderId) {

		ModelAndView result;
		Collection<Message> messages;
		Collection<Folder> folders;

		final Actor actor = this.actorService.findByPrincipal();
		final Folder folder = this.folderService.findOne(folderId);
		final String folderName = this.folderService.findOne(folderId).getName();

		messages = this.messageService.getMessagesByFolder(folder);
		folders = actor.getFolders();

		result = new ModelAndView("message/actor/list");
		result.addObject("folderName", folderName);
		result.addObject("folders", folders);
		result.addObject("messages", messages);

		return result;

	}

	//Create ---------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView res;

		final Message newMessage = this.messageService.create();
		res = this.createEditModelAndView(newMessage);

		return res;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Message newMessage, final BindingResult binding) {

		ModelAndView res;

		if (binding.hasErrors())
			res = this.createEditModelAndView(newMessage);
		//System.out.println(binding.getAllErrors());
		else

			try {
				this.messageService.save(newMessage);
				res = new ModelAndView("redirect:/folder/actor/listSystem.do");
				//System.out.println("------------Errors:" + binding.getAllErrors());
			} catch (final Throwable error) {
				res = this.createEditModelAndView(newMessage, "folder.error.save");

			}
		return res;
	}
	//Display ------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer messageId) {
		ModelAndView result;
		final Message newMessage = this.messageService.findOne(messageId);

		result = new ModelAndView("message/actor/display");
		result.addObject("newMessage", newMessage);

		return result;
	}

	@RequestMapping(value = "/move", method = RequestMethod.GET)
	public ModelAndView move(@RequestParam final int messageId) {
		ModelAndView result;
		Boolean owner = false;
		Message mensaje = new Message();
		Assert.notNull(mensaje);
		try {
			mensaje = this.messageService.findOne(messageId);
			final Actor a = this.actorService.findByPrincipal();
			if (a.getFolders().contains(this.messageService.findFolderByMessage(mensaje)))
				owner = true;
			Collection<Folder> folders;
			folders = new ArrayList<Folder>(a.getFolders());
			folders.remove(this.messageService.findFolderByMessage(mensaje));
			result = new ModelAndView("message/actor/move");
			result.addObject("owner", owner);
			result.addObject("messageId", mensaje.getId());
			result.addObject("folders", folders);
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(mensaje, "message.commit.error");
		}
		return result;
	}
	@RequestMapping(value = "/moveMessage", method = RequestMethod.GET)
	public ModelAndView moveMessage(@RequestParam final int folderId, @RequestParam final int messageId) {
		ModelAndView res;
		try {
			final Message message = this.messageService.findOne(messageId);
			final Folder folder = this.folderService.findOne(folderId);
			this.messageService.move(message, folder);
			res = new ModelAndView("redirect:list.do?folderId=" + folderId);
		} catch (final Throwable error) {
			final Actor actor = this.actorService.findByPrincipal();
			final Collection<Folder> folders = actor.getFolders();
			res = new ModelAndView("redirect:list.do?folderId");
			res = new ModelAndView("message/actor/move");
			res.addObject("folders", folders);
			res.addObject("requestURI", "message/actor/move.do?messageId=" + messageId);
			res.addObject("message", "message.commit.error");
		}
		return res;

	}

	//Delete-----------------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int newMessageId) {

		ModelAndView res;

		try {

			final Message message = this.messageService.findOne(newMessageId);
			final Folder folder = this.messageService.findFolderByMessage(message);
			final Integer folderId = folder.getId();

			this.messageService.delete(message);
			res = new ModelAndView("redirect:list.do?folderId=" + folderId);

		} catch (final Throwable error) {
			final Actor actor = this.actorService.findByPrincipal();
			final Collection<Folder> folders = actor.getFolders();
			res = new ModelAndView("redirect:list.do?folderId");
			res = new ModelAndView("message/actor/move");
			res.addObject("folders", folders);
			res.addObject("requestURI", "message/actor/move.do?newMessageId=" + newMessageId);
			res.addObject("newMessage", "message.commit.error");
		}
		return res;

	}

	//Ancillary methods-------------------------------------------------------------
	private ModelAndView createEditModelAndView(final Message newMessage) {
		return this.createEditModelAndView(newMessage, null);
	}

	private ModelAndView createEditModelAndView(final Message newMessage, final String text) {
		ModelAndView res;

		final Collection<Actor> recipients = this.actorService.findAll();
		final Actor actor = this.actorService.findByPrincipal();
		recipients.remove(actor);

		res = new ModelAndView("message/actor/create");
		res.addObject("newMessage", newMessage);
		res.addObject("text", text);
		res.addObject("recipients", recipients);

		return res;
	}
}
