
package controllers.sponsor;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.SponsorService;
import controllers.AbstractController;
import domain.Sponsor;

@Controller
@RequestMapping("/sponsor")
public class SponsorController extends AbstractController {

	@Autowired
	private SponsorService	sponsorService;

	@Autowired
	private ActorService	actorService;


	//List ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {

		final Collection<Sponsor> sponsors;

		sponsors = this.sponsorService.findAll();

		final ModelAndView result = new ModelAndView("sponsor/list");
		result.addObject("sponsors", sponsors);
		result.addObject("message", message);
		result.addObject("requestURI", "sponsor/list.do");

		return result;
	}

	// Create -------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Sponsor sponsor = this.sponsorService.create();

		final ModelAndView result = this.createEditModelAndView(sponsor);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Sponsor sponsor, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(sponsor);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.sponsorService.save(sponsor);

				result = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				result = this.createEditModelAndView(sponsor, "sponsor.commit.error");
			}
		return result;

	}
	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView save() {

		ModelAndView result;
		final Sponsor sponsor = this.sponsorService.findOne(this.actorService.findByPrincipal().getId());
		result = this.createEditModelAndView(sponsor);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Sponsor sponsor, final BindingResult binding) {

		ModelAndView result;
		if (binding.hasErrors()) {
			result = this.createEditModelAndView(sponsor);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.sponsorService.save(sponsor);
				result = new ModelAndView("redirect:/");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(sponsor, "sponsor.commit.error");
				System.out.println(oops.getLocalizedMessage());
			}

		return result;
	}

	//Ancillary methods ------------------------------------------------------------

	protected ModelAndView createEditModelAndView(final Sponsor sponsor) {

		ModelAndView result;
		result = this.createEditModelAndView(sponsor, null);
		return result;

	}

	protected ModelAndView createEditModelAndView(final Sponsor sponsor, final String message) {

		ModelAndView result;
		result = new ModelAndView("sponsor/create");
		result.addObject("sponsor", sponsor);
		result.addObject("message", message);
		return result;
	}
}
