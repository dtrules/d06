
package controllers;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ReportService;
import domain.Report;

@Controller
@RequestMapping("/report")
public class ReportController extends AbstractController {

	//Constructors ----------------------

	public ReportController() {
		super();
	}


	//Services -----------------------------

	@Autowired
	private ReportService	reportService;


	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		Collection<Report> reports = new ArrayList<Report>();

		reports = this.reportService.findReportByFinalMode();

		final ModelAndView result = new ModelAndView("report/list");
		result.addObject("reports", reports);
		result.addObject("requestURI", "report/list.do");

		return result;
	}

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer reportId) {
		ModelAndView result;
		Report report;

		report = this.reportService.findOne(reportId);
		result = new ModelAndView("report/display");
		result.addObject("report", report);
		result.addObject("reportId", report.getId());

		return result;
	}

}
