package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Customer;
import domain.Finder;

import services.ComplaintService;
import services.FinderService;

@Controller
@RequestMapping("/finder")
public class FinderController extends AbstractController {

	// Constructors --------------------------------------

	public FinderController() {
		super();

	}

	// Services ----------------------------------------------

	@Autowired
	private FinderService finderService;

	// Display ---------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer finderId) {
		ModelAndView result;
		Finder finder;

		finder = this.finderService.findOne(finderId);

		result = new ModelAndView("finder/display");
		result.addObject("finder", finder);

		return result;
	}

	// Edit ------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int finderId) {
		ModelAndView res;
		Finder finder;

		finder = this.finderService.findOne(finderId);
		Assert.notNull(finder);
		res = createEditModelAndView(finder);

		return res;
	}

	// Save del Edit----------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Finder finder,
			final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(finder);
		} else
			try {
				this.finderService.save(finder);
				res = new ModelAndView("redirect:/finder/list.do");

			} catch (final Throwable oops) {
				res = this.createEditModelAndView(finder,
						"finder.commit.error");
			}

		return res;
	}

	// List ----------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<Finder> finders;

		finders = this.finderService.findAll();

		res = new ModelAndView("finder/list");
		res.addObject("requestURI", "finder/list.do");
		res.addObject("finders", finders);

		return res;
	}

	// Ancillary methods
	// ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Finder finder) {

		return this.createEditModelAndView(finder, null);
	}

	private ModelAndView createEditModelAndView(final Finder finder,
			final String message) {

		final ModelAndView res = new ModelAndView("finder/edit");
		res.addObject("finder", finder);
		res.addObject("message", message);

		return res;

	}
}
