
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ApplicationRepository;
import security.Authority;
import domain.Application;
import domain.CreditCard;
import domain.HandyWorker;
import domain.Status;
import domain.Task;

@Service
@Transactional
public class ApplicationService {

	//Managed repository ----------------------------

	@Autowired
	private ApplicationRepository	applicationRepository;

	//Supported Services ----------------------------

	@Autowired
	private ActorService			actorService;

	@Autowired
	private TaskService				taskService;

	@Autowired
	private HandyWorkerService		handyWorkerService;

	@Autowired
	private CustomerService			customerService;


	// Contructor methods
	public ApplicationService() {
		super();
	}

	// Simple CRUD methods

	public Application create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);
		final Integer cvvCode = 0;
		final Application res = new Application();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		final Date moment = new Date();
		res.setMoment(moment);

		final Status status = new Status();
		status.setValue("PENDING");
		res.setStatus(status);

		final Task task = new Task();
		res.setTask(task);

		final CreditCard creditCard = new CreditCard();
		creditCard.setCvvCode(cvvCode);
		res.setCreditCard(creditCard);

		Assert.notNull(res);

		return res;
	}

	public Collection<Application> findAll() {
		final Collection<Application> res = this.applicationRepository.findAll();

		Assert.notNull(res);

		return res;
	}

	public Application findOne(final int applicationId) {
		Assert.isTrue(applicationId > 0);

		final Application res = this.applicationRepository.findOne(applicationId);

		Assert.notNull(res);

		return res;
	}

	public Application save(final Application application) {
		Assert.notNull(application);

		Application res;
		if (application.getId() != 0) {
			this.checkApplication(application);
			this.actorService.checkAuth(Authority.HANDYWORKER, Authority.CUSTOMER);
			if (this.customerService.findByPrincipal() != null)
				if (application.getStatus().getValue().equals("ACCEPTED")) {
					Assert.notNull(application.getCreditCard());
					Assert.isTrue(application.getCreditCard().getCvvCode() > 100);
					Assert.isTrue(application.getCreditCard().getCvvCode() < 999);

				}
			res = this.applicationRepository.save(application);
		} else {

			this.actorService.checkAuth(Authority.HANDYWORKER);
			final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

			final Task task = application.getTask();
			res = this.applicationRepository.save(application);
			task.getApplications().add(res);
			handyWorker.getApplications().add(res);
		}
		return res;
	}
	public void delete(final Application application) {
		Assert.notNull(application);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(application.getId() != 0);
		Assert.isTrue(this.applicationRepository.exists(application.getId()));

		this.applicationRepository.delete(application);
	}

	public void acceptApplication(final int applicationId) {
		Assert.isTrue(applicationId != 0);
		final Application application = this.findOne(applicationId);
		Assert.notNull(application);

		this.actorService.checkAuth(Authority.CUSTOMER);

		final Status status = new Status();
		status.setValue("ACCEPTED");
		application.setStatus(status);
		this.applicationRepository.save(application);
	}

	public void rejectApplication(final int applicationId) {
		Assert.isTrue(applicationId != 0);
		final Application application = this.findOne(applicationId);
		Assert.notNull(application);

		this.actorService.checkAuth(Authority.CUSTOMER);

		final Status status = new Status();
		status.setValue("REJECTED");
		application.setStatus(status);
		this.applicationRepository.save(application);

	}

	//Check Application
	public void checkApplication(final Application application) {
		Boolean res = true;

		if (application.getStatus() == null || application.getTask() == null || application.getComments() == null || application.getMoment() == null)
			res = false;

		Assert.isTrue(res);
	}

	public Collection<Application> getApplicationsByTask(final int taskId) {
		return this.applicationRepository.getApplicationsByTask(taskId);
	}

	public Collection<Application> getApplicationsByTasks(final Collection<Task> tasks) {
		final Collection<Application> result = new ArrayList<Application>();

		for (final Task t : tasks)
			result.addAll(this.getApplicationsByTask(t.getId()));
		return result;
	}

	public Double minPricePerApplication() {
		Double res;
		res = this.applicationRepository.minPricePerApplication();
		return res;
	}

	public Double maxPricePerApplication() {
		Double res;
		res = this.applicationRepository.maxPricePerApplication();
		return res;
	}

	public Double avgPricePerApplication() {
		Double res;
		res = this.applicationRepository.avgPricePerApplication();
		return res;
	}

	public Double deviationPricePerApplication() {
		Double res;
		res = this.applicationRepository.deviationPricePerApplication();
		return res;
	}

	public Double ratioOfPendingApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfPendingApplications();
		return res;
	}

	public Double ratioOfAcceptedApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfAcceptedApplications();
		return res;
	}

	public Double ratioOfRejectedApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfRejectedApplications();
		return res;
	}

	public Double ratioOfPendingApplicationsElapsed() {
		Double res;
		res = this.applicationRepository.findRatioOfPendingApplicationsElapsed();
		return res;
	}
}
