
package services;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MiscellaneousRecordRepository;
import security.Authority;
import domain.Curriculum;
import domain.HandyWorker;
import domain.MiscellaneousRecord;

@Service
@Transactional
public class MiscellaneousRecordService {

	// Managed repository ----------------------------------------

	@Autowired
	private MiscellaneousRecordRepository	miscellaneousRecordRepository;

	@Autowired
	private HandyWorkerService				handyWorkerService;

	//Supported Services -----------------------------------

	@Autowired
	private ActorService					actorService;


	// Constructor methods
	public MiscellaneousRecordService() {
		super();
	}

	public MiscellaneousRecord create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final MiscellaneousRecord res = new MiscellaneousRecord();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		Assert.notNull(res);

		return res;
	}

	public Collection<MiscellaneousRecord> findAll() {
		final Collection<MiscellaneousRecord> res = this.miscellaneousRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}
	public MiscellaneousRecord findOne(final int miscellaneousRecordId) {
		Assert.isTrue(miscellaneousRecordId > 0);

		final MiscellaneousRecord res = this.miscellaneousRecordRepository.findOne(miscellaneousRecordId);

		Assert.notNull(res);

		return res;
	}

	public MiscellaneousRecord save(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

		final Curriculum curriculum = handyWorker.getCurriculum();
		final Collection<MiscellaneousRecord> miscellaneousRecords = curriculum.getMiscellaneousRecords();

		this.checkMiscellaneousRecord(miscellaneousRecord);
		this.actorService.checkSpamWords(miscellaneousRecord.getAttachment());
		this.actorService.checkSpamWords(miscellaneousRecord.getTitle());
		for (final String s : miscellaneousRecord.getComments())
			this.actorService.checkSpamWords(s);
		final MiscellaneousRecord res = this.miscellaneousRecordRepository.save(miscellaneousRecord);

		miscellaneousRecords.add(res);
		this.handyWorkerService.save(handyWorker);

		return res;
	}

	public void delete(final MiscellaneousRecord miscellaneousRecord) {
		Assert.notNull(miscellaneousRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		Assert.isTrue(miscellaneousRecord.getId() > 0);

		this.miscellaneousRecordRepository.delete(miscellaneousRecord);
	}

	//Check MiscellaneousRecord
	public void checkMiscellaneousRecord(final MiscellaneousRecord miscellaneousRecord) {
		Boolean res = true;

		if (miscellaneousRecord.getComments() == null)
			res = false;

		Assert.isTrue(res);
	}
}
