
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.PersonalRecordRepository;
import security.Authority;
import domain.PersonalRecord;

@Service
@Transactional
public class PersonalRecordService {

	//Managed repository ---------------------------------------------------

	@Autowired
	private PersonalRecordRepository	personalRecordRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService				actorService;


	// Contructor methods
	public PersonalRecordService() {
		super();
	}

	public PersonalRecord create() {
		final PersonalRecord res = new PersonalRecord();

		Assert.notNull(res);

		return res;
	}

	public Collection<PersonalRecord> findAll() {
		final Collection<PersonalRecord> res = this.personalRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}

	public PersonalRecord findOne(final int personalRecordId) {
		Assert.isTrue(personalRecordId > 0);

		final PersonalRecord res = this.personalRecordRepository.findOne(personalRecordId);

		Assert.notNull(res);

		return res;
	}

	public PersonalRecord save(final PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);

		this.checkPersonalRecord(personalRecord);
		this.actorService.checkSpamWords(personalRecord.getEmail());
		this.actorService.checkSpamWords(personalRecord.getLinkedinProfile());
		this.actorService.checkSpamWords(personalRecord.getName());
		this.actorService.checkSpamWords(personalRecord.getPhoto());
		final PersonalRecord res = this.personalRecordRepository.save(personalRecord);

		return res;
	}

	public void delete(final PersonalRecord personalRecord) {
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.notNull(personalRecord);
		Assert.isTrue(personalRecord.getId() > 0);
		this.personalRecordRepository.delete(personalRecord);
	}

	// Check PersonalRecord
	public void checkPersonalRecord(final PersonalRecord personalRecord) {
		Boolean res = true;

		if (personalRecord == null)
			res = false;

		Assert.isTrue(res);
	}
}
