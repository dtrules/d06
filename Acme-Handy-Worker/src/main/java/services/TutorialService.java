
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TutorialRepository;
import security.Authority;
import domain.HandyWorker;
import domain.Section;
import domain.Sponsorship;
import domain.Tutorial;

@Service
@Transactional
public class TutorialService {

	//Managed repository ------------------------------------------------

	@Autowired
	private TutorialRepository	tutorialRepository;

	@Autowired
	private SponsorshipService	sponsorshipService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	// Constructor methods ---------------------------------------------------------
	public TutorialService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Tutorial create() {

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Tutorial t = new Tutorial();

		final Collection<Section> sections = new ArrayList<Section>();

		t.setSections(sections);

		return t;
	}

	public Tutorial save(final Tutorial t) {
		Assert.notNull(t);
		this.checkTutorial(t);

		this.actorService.checkAuth(Authority.HANDYWORKER);
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

		final Date moment = new Date(System.currentTimeMillis() - 1000);

		t.setMoment(moment);
		Tutorial result;
		this.actorService.checkSpamWords(t.getSummary());
		this.actorService.checkSpamWords(t.getTitle());
		this.actorService.checkSpamWords(t.getPicture());

		result = this.tutorialRepository.save(t);
		handyWorker.getTutorials().add(result);
		return result;
	}

	public void delete(final Tutorial t) {

		Assert.notNull(t);
		Assert.isTrue(t.getId() != 0);

		this.actorService.checkAuth(Authority.HANDYWORKER);
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		handyWorker.getTutorials().remove(t);
		t.getSections().removeAll(t.getSections());
		final Collection<Sponsorship> sponsorships = this.sponsorshipService.findByTutorial(t.getId());
		Assert.notEmpty(sponsorships);

		this.tutorialRepository.delete(t);

	}
	public Tutorial findOne(final int tutorialId) {
		Assert.isTrue(tutorialId != 0);
		Tutorial result;

		result = this.tutorialRepository.findOne(tutorialId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Tutorial> findAll() {

		Collection<Tutorial> result;

		result = this.tutorialRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Tutorial
	public void checkTutorial(final Tutorial tutorial) {
		Boolean result = true;

		if (tutorial.getTitle() == null || tutorial.getSummary() == null || tutorial.getPicture() == null || tutorial.getSections() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Tutorial findTutorialBySection(final int sectionId) {
		return this.tutorialRepository.findTutorialBySection(sectionId);
	}

	public Collection<Tutorial> findTutorialWithSections() {
		final Collection<Tutorial> res = new ArrayList<Tutorial>();

		for (final Tutorial t : this.tutorialRepository.findAll())
			if (!t.getSections().isEmpty())
				res.add(t);
		return res;
	}

}
