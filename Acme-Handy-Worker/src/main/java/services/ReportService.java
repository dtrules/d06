
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ReportRepository;
import security.Authority;
import domain.Complaint;
import domain.Note;
import domain.Referee;
import domain.Report;

@Service
@Transactional
public class ReportService {

	//Managed Repository------------------------------

	@Autowired
	private ReportRepository	reportRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService		actorService;

	@Autowired
	private RefereeService		refereeService;


	// Constructor methods ---------------------------------------------------------
	public ReportService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Report create() {

		this.actorService.checkAuth(Authority.REFEREE);

		final Report r = new Report();

		final Date moment = new Date(System.currentTimeMillis() - 1000);
		r.setMoment(moment);
		r.setDraftMode(true);

		final Collection<Note> notes = new ArrayList<Note>();
		r.setNotes(notes);

		return r;
	}

	public Report save(final Report r, final Complaint complaint) {
		Assert.notNull(r);

		final Referee referee = this.refereeService.findByPrincipal();
		Assert.notNull(referee);
		r.setComplaint(complaint);

		this.checkReport(r);

		Report result;
		this.actorService.checkSpamWords(r.getAttachment());

		this.actorService.checkSpamWords(r.getDescription());

		result = this.reportRepository.save(r);
		referee.getReports().add(result);
		this.refereeService.save(referee);

		return result;
	}

	public Report saveEdit(final Report r) {
		Report result;
		Assert.notNull(r);
		final Referee referee = this.refereeService.findByPrincipal();
		Assert.notNull(referee);
		final Report db = this.findOne(r.getId());
		Assert.isTrue(db.getDraftMode() == true); //Se comprueba que legalText no est� almacenado en finalMode
		result = this.reportRepository.save(r);

		return result;

	}

	public void delete(final Report t) {
		Assert.notNull(t);
		Assert.isTrue(t.getId() != 0);

		this.actorService.checkAuth(Authority.REFEREE);
		final Referee referee = this.refereeService.findByPrincipal();

		Assert.isTrue(t.getDraftMode());
		final Collection<Report> reports = referee.getReports();
		reports.remove(t);
		referee.setReports(reports);
		final Collection<Note> notes = t.getNotes();
		notes.removeAll(notes);
		t.setNotes(notes);
		this.refereeService.save(referee);
		this.reportRepository.delete(t);

	}

	public Report findOne(final int reportId) {
		Assert.isTrue(reportId != 0);
		Report result;

		result = this.reportRepository.findOne(reportId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Report> findAll() {

		Collection<Report> result;

		result = this.reportRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Report
	public void checkReport(final Report report) {
		Boolean result = true;

		if (report.getComplaint() == null || report.getMoment() == null || report.getDescription() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Report findReportByNote(final int noteId) {
		return this.reportRepository.findReportByNote(noteId);
	}

	public Collection<Report> findReportByFinalMode() {
		return this.reportRepository.findReportsByFinalMode();

	}

	//Queries-----------------------------------------
	public Collection<Double> avgNotePerRefereeReport(final Report report) {
		return this.reportRepository.avgNotePerRefereeReport(report);
	}

}
