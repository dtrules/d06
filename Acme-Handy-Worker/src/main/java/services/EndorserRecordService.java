
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.EndorserRecordRepository;
import security.Authority;
import domain.Curriculum;
import domain.EndorserRecord;
import domain.HandyWorker;

@Service
public class EndorserRecordService {

	//Managed repository -------------------------------------

	@Autowired
	private EndorserRecordRepository	endorserRecordRepository;

	//Supported Services -----------------------------------

	@Autowired
	private ActorService				actorService;

	@Autowired
	private HandyWorkerService			handyWorkerService;


	// Constructor methods
	public EndorserRecordService() {
		super();
	}

	public EndorserRecord create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final EndorserRecord res = new EndorserRecord();

		final Collection<String> comments = new ArrayList<String>();
		res.setComments(comments);

		Assert.notNull(res);

		return res;
	}

	public Collection<EndorserRecord> findAll() {
		final Collection<EndorserRecord> res = this.endorserRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}
	public EndorserRecord findOne(final int endorserRecordId) {
		Assert.isTrue(endorserRecordId > 0);

		final EndorserRecord res = this.endorserRecordRepository.findOne(endorserRecordId);

		Assert.notNull(res);

		return res;
	}

	public EndorserRecord save(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

		final Curriculum curriculum = handyWorker.getCurriculum();
		final Collection<EndorserRecord> endorserRecords = curriculum.getEndorserRecords();

		this.checkEndorserRecord(endorserRecord);
		for (final String c : endorserRecord.getComments())
			this.actorService.checkSpamWords(c);

		this.actorService.checkSpamWords(endorserRecord.getEmail());
		this.actorService.checkSpamWords(endorserRecord.getLinkedinProfile());
		this.actorService.checkSpamWords(endorserRecord.getName());

		final EndorserRecord res = this.endorserRecordRepository.save(endorserRecord);

		endorserRecords.add(res);
		this.handyWorkerService.save(handyWorker);
		return res;
	}

	public void delete(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(endorserRecord.getId() > 0);

		this.endorserRecordRepository.delete(endorserRecord);
	}

	//Check EndorserRecord
	public void checkEndorserRecord(final EndorserRecord endorserRecord) {
		Boolean res = true;

		if (endorserRecord.getComments() == null)
			res = false;

		Assert.isTrue(res);
	}
}
