
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SectionRepository;
import security.Authority;
import domain.HandyWorker;
import domain.Section;
import domain.Tutorial;

@Service
@Transactional
public class SectionService {

	//Managed repository ------------------------------------------------

	@Autowired
	private SectionRepository	sectionRepository;

	//Supported services ------------------------------------------------
	@Autowired
	private ActorService		actorService;
	@Autowired
	private TutorialService		tutorialService;
	@Autowired
	private HandyWorkerService	handyWorkerService;


	// Constructor methods ---------------------------------------------------------
	public SectionService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Section create() {

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Section s = new Section();

		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		Assert.notNull(handyWorker);

		return s;
	}

	public Section save(final Section s, final Tutorial t) {
		Assert.notNull(s);
		this.checkSection(s);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Section section;
		this.actorService.checkSpamWords(s.getText());
		this.actorService.checkSpamWords(s.getTitle());
		this.actorService.checkSpamWords(s.getPicture());

		section = this.sectionRepository.save(s);
		Assert.notNull(t);
		final Collection<Section> sections = t.getSections();
		sections.add(section);
		t.setSections(sections);
		this.tutorialService.save(t);

		return section;
	}

	public void delete(final Section s) {

		Assert.notNull(s);
		Assert.isTrue(s.getId() != 0);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Section result = this.sectionRepository.findOne(s.getId());

		this.sectionRepository.delete(result);

	}

	public Section findOne(final int sectionId) {
		Assert.isTrue(sectionId != 0);
		Section result;

		result = this.sectionRepository.findOne(sectionId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Section> findAll() {

		Collection<Section> result;

		result = this.sectionRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Section
	public void checkSection(final Section section) {
		Boolean result = true;

		if (section.getTitle() == null || section.getText() == null || section.getPicture() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Collection<Section> findSectionsWithoutTutorial() {
		final Collection<Section> sections = this.sectionRepository.findAll();
		for (final Section s : this.sectionRepository.findAll())
			if (this.tutorialService.findTutorialBySection(s.getId()) != null)
				sections.remove(s);
		return sections;
	}

}
