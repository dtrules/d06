
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Curriculum;
import domain.EducationRecord;
import domain.EndorserRecord;
import domain.MiscellaneousRecord;
import domain.PersonalRecord;
import domain.ProfessionalRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class CurriculumServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private CurriculumService	curriculumService;
	@Autowired
	private HandyWorkerService	handyWorkerService;


	// Test ------------------------

	// Caso en que se crea un Curriculum correctamente y guarda datos

	public void createCurriculum(final String username, final Collection<MiscellaneousRecord> miscellaneousRecords, final Collection<EndorserRecord> endorserRecords, final Collection<EducationRecord> educationRecords,
		final Collection<ProfessionalRecord> professionalRecords, final PersonalRecord personalRecord, final Class<?> expected) {

		Class<?> caught = null;

		try {
			super.authenticate(username);

			// Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final Curriculum curriculum = this.curriculumService.create();
			curriculum.setEducationRecords(educationRecords);
			curriculum.setEndorserRecords(endorserRecords);
			curriculum.setMiscellaneousRecords(miscellaneousRecords);
			curriculum.setPersonalRecord(personalRecord);
			curriculum.setProfessionalRecords(professionalRecords);

			this.curriculumService.save(curriculum);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateCurriculum() {

		final Object testingData[][] = {
			// Crear y guardar un curriculum con handyworker autenticado
			{
				"handyWorker1", Collections.<MiscellaneousRecord> emptySet(), Collections.<EndorserRecord> emptySet(), Collections.<EducationRecord> emptySet(), Collections.<ProfessionalRecord> emptySet(), new PersonalRecord(), null
			},
			// Crear y guardar una application con un handy worker y con un
			// ticker mal introducido
			{
				"customer1", Collections.<MiscellaneousRecord> emptySet(), Collections.<EndorserRecord> emptySet(), Collections.<EducationRecord> emptySet(), Collections.<ProfessionalRecord> emptySet(), new PersonalRecord(), IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createCurriculum((String) testingData[i][0], (Collection<MiscellaneousRecord>) testingData[i][1], (Collection<EndorserRecord>) testingData[i][2], (Collection<EducationRecord>) testingData[i][3],
				(Collection<ProfessionalRecord>) testingData[i][4], (PersonalRecord) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	// Caso en el que borramos un Curriculum

	public void deleteCurriculum(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			// Buscamos el task
			final Curriculum curriculum = (Curriculum) this.curriculumService.findAll().toArray()[0];

			// Borramos
			this.curriculumService.delete(curriculum);

			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteCurriculumTest() {
		final Object testingData[][] = {
			// Se accede con handy worker -> true
			{
				"handyWorker1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCurriculum((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
