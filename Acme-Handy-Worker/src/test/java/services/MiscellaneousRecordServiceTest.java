
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.MiscellaneousRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class MiscellaneousRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private MiscellaneousRecordService	miscellaneousRecordService;
	@Autowired
	private HandyWorkerService			handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un MiscellaneousRecord correctamente y guarda datos

	public void createMiscellaneousRecord(final String username, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.create();
			miscellaneousRecord.setComments(comments);

			this.miscellaneousRecordService.save(miscellaneousRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateMiscellaneousRecord() {

		final Object testingData[][] = {
			// Crear y guardar una MiscellaneousRecord con handy worker autenticado
			{
				"handyWorker1", Collections.<String> emptySet(), null
			},
			// Crear y guardar una MiscellaneousRecord con otro actor
			{
				"customer1", Collections.<String> emptySet(), IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createMiscellaneousRecord((String) testingData[i][0], (Collection<String>) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	//Caso en el que borramos un miscellaneousRecord

	public void deleteMiscellaneousRecord(final String username, final int miscellaneousRecordId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el miscellaneousRecord
			final MiscellaneousRecord miscellaneousRecord = this.miscellaneousRecordService.findOne(miscellaneousRecordId);

			//Borramos
			this.miscellaneousRecordService.delete(miscellaneousRecord);

			//Comprobamos que se ha borrado

			Assert.isNull(this.miscellaneousRecordService.findOne(miscellaneousRecordId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
