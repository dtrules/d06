
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.Authority;
import utilities.AbstractTest;
import domain.Warranty;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class WarrantyServiceTest extends AbstractTest {

	//Service under test------------------------
	@Autowired
	private WarrantyService	warrantyService;

	@Autowired
	private ActorService	actorService;


	//Caso en que se crea un Task correctamente y guarda datos

	public void createWarranty(final String username, final String title, final String laws, final String terms, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es admin
			this.actorService.checkAuth(Authority.ADMIN);

			final Warranty warranty = this.warrantyService.create();
			warranty.setTitle(title);
			warranty.setLaws(laws);
			warranty.setTerms(terms);
			warranty.setDraftMode(true);

			this.warrantyService.save(warranty);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateWarranty() {

		final Object testingData[][] = {
			// Crear y guardar una warranty con admin autenticado -> true
			{
				"admin", "Title1", "Laws1", "Terms1", null
			},
			// Crear y guardar una warranty con un actor diferente (sponsor1) -> false
			{
				"sponsor2", "Title1", "Laws1", "Terms1", IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createWarranty((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	//Caso en el que borramos una Task

	public void deleteWarranty(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el warranty
			final Warranty warranty = (Warranty) this.warrantyService.findAll().toArray()[0];

			//Borramos
			this.warrantyService.delete(warranty);

			//Comprobamos que se ha borrado

			Assert.isNull(this.warrantyService.findOne(warranty.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	//	@Test
	//	public void driverDeleteWarranty() {
	//
	//		final Object testingData[][] = {
	//			// borrar una warranty de la base de datos siendo administrator -> true
	//			{
	//				"admin", null
	//			},
	//			// borrar una warranty de la base de datos siendo un actor diferente -> false
	//			{
	//				"sponsor2", IllegalArgumentException.class
	//			},
	//			//borrar una warranty de un administrator que no es suya -> false
	//			{
	//				"admin", IllegalArgumentException.class
	//			},
	//
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.deleteWarranty((String) testingData[i][0], (Class<?>) testingData[i][1]);
	//	}

}
